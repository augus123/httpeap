using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace ToolClass
{
    public static class SQLiteHelper
    {
        public static string ConStr ="";

        /// <summary>
        /// 创建数据库
        /// </summary>
        public static bool NewDbFile(string dbPath)
        {
            try
            {
                SQLiteConnection.CreateFile(dbPath);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("新建数据库文件" + dbPath + "失败：\r\n" + ex.Message);
            }
        }
        /// <summary>
        /// 创建表
        /// </summary>
        /// <param name="dbPath">指定数据库文件</param>
        /// <param name="tableName">表名称</param>
        public static void NewTable(string dbPath, string sqllite)
        {
            SQLiteConnection sqliteConn = new SQLiteConnection("Data Source=" + dbPath);
            if (sqliteConn.State != System.Data.ConnectionState.Open)
            {
                sqliteConn.Open();
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = sqliteConn;              
                cmd.CommandText = sqllite;
                cmd.ExecuteNonQuery();
            }  //Alter table tabname add primary key(col) 
            sqliteConn.Close();
        }

        /// <summary>
        /// 判断数据库中表格是否存在
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="path">数据库路径</param>
        /// <returns>是否存在</returns>
        public static bool ExistTable(string tableName, string path)
        {
            if (System.IO.File.Exists(path) == false)
                SQLiteConnection.CreateFile(path);

            using (SQLiteConnection con = new SQLiteConnection(string.Format("Data Source={0};Version=3;", path)))
            {
                con.Open();
                int result;
                string count = "0";
                //开启事务
                using (SQLiteTransaction tr = con.BeginTransaction())
                {
                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        string existSql = String.Format("select count(*)  from sqlite_master where type='table' and name = '{0}'", tableName);

                        cmd.Transaction = tr;
                        cmd.CommandText = existSql;
                        //使用result = cmd.ExecuteNonQuery();这句判断返回值的方法不正确，不论是否存在返回值都为-1

                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            count = reader[0].ToString();
                        }
                    }
                    //提交事务
                    tr.Commit();
                }
                con.Close();
                if (count == "0")
                    return false;//没有该表格
                else
                    return true;
            }
        }
        
        /// <summary>
        /// 执行增删改
        /// </summary>
        /// <param name="sql"><
        /// ram>
        /// <returns></returns>
        public static int Update(string sql)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DBConnection.Close();
            }
        }
        public static int Updates(string sql, SQLiteParameter[] param)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                cmd.Parameters.AddRange(param);//添加参数
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLog("执行Update(string sql)方法发生错误，错误日志：" + ex.Message);
                throw;
            }
            finally
            {
                DBConnection.Close();
            }
        }
        /// <summary>
        /// 获取单一结果
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object GetSingleResult(string sql)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                DBConnection.Close();
            }
        }
        /// <summary>
        /// 返回结果集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static SQLiteDataReader GetReader(string sql)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                DBConnection.Close();
                throw ex;
            }


        }
        /// <summary>
        /// 返回数据集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string sql)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
             DataSet ds = new DataSet();
            try
            {
                DBConnection.Open();
                da.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                DBConnection.Close();
            }
        }


        #region 启用事务执行多条SQL语句
        /// <summary>
        /// 启用事务执行多条SQL语句
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        public static bool UpdateByTran(List<string> sqlList)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.Connection = DBConnection;
            try
            {
                DBConnection.Open();
                cmd.Transaction = DBConnection.BeginTransaction();//开启事务
                foreach (string itemsql in sqlList)
                {
                    cmd.CommandText = itemsql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();//提交事务
                return true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                throw new Exception("调用事务方法时出现错误：" + ex.Message);
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;//清空事务
                DBConnection.Close();

            }
        }
        #endregion

        #region 错误信息写入日志
        /// <summary>
        /// 将错误信息写入日志文件
        /// </summary>
        /// <param name="msg"></param>
        private static void WriteLog(string msg)
        {
            FileStream fs = new FileStream("Log.text", FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine("[{0}]  错误信息：{1}", DateTime.Now.ToString(), msg);
            sw.Close();
            fs.Close();
        }
        #endregion

        #region 执行带参数的SQL语句
        /// <summary>
        /// 执行增删改
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static int Update(string sql, SQLiteParameter[] param)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                cmd.Parameters.AddRange(param);//添加参数
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLog("执行Update(string sql)方法发生错误，错误日志：" + ex.Message);
                throw;
            }
            finally
            {
                DBConnection.Close();
            }
        }
        /// <summary>
        /// 返回单一结果
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object GetSingleResult(string sql, SQLiteParameter[] param)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                cmd.Parameters.AddRange(param);//添加参数
                return cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                WriteLog("执行GetSingleResult(string sql)方法发生错误，错误日志：" + ex.Message);
                throw;
            }
            finally
            {
                DBConnection.Close();
            }
        }
        /// <summary>
        /// 返回数据集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static SQLiteDataReader GetReader(string sql, SQLiteParameter[] param)
        {
            SQLiteConnection DBConnection = new SQLiteConnection(ConStr);
            SQLiteCommand cmd = new SQLiteCommand(sql, DBConnection);
            try
            {
                DBConnection.Open();
                cmd.Parameters.AddRange(param);
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                WriteLog("执行GetReader(string sql)方法发生错误，错误日志：" + ex.Message);
                DBConnection.Close();
                throw ex;
            }

        }
        #endregion


    }

    public class CSVHelper
    {
        public void DataToCsv(System.Data.DataSet dt, string strName)
        {
            System.Windows.Forms.SaveFileDialog saveDialog;//保存文件的对话框
            saveDialog = new System.Windows.Forms.SaveFileDialog();
            saveDialog.DefaultExt = "csv";
            saveDialog.Filter = "txt文件|*.csv";
            saveDialog.Title = strName;
            DialogResult dialog = saveDialog.ShowDialog();
            string strPath;
            if (DialogResult.OK == dialog)
            {
                strPath = saveDialog.FileName;


                StringBuilder strColu = new StringBuilder();
                StringBuilder strValue = new StringBuilder();
                int i = 0;
                try
                {
                    StreamWriter sw = new StreamWriter(new FileStream(strPath, FileMode.CreateNew), Encoding.GetEncoding("GB2312"));
                    //先打印表头
                    for (i = 0; i <= dt.Tables[0].Columns.Count - 1; i++)
                    {
                        strColu.Append(dt.Tables[0].Columns[i].ColumnName);
                        strColu.Append(",");
                    }
                    strColu.Remove(strColu.Length - 1, 1);//移除掉最后一个,字符
                    sw.WriteLine(strColu);
                    //再逐行打印行数据
                    foreach (DataRow dr in dt.Tables[0].Rows)
                    {
                        strValue.Remove(0, strValue.Length);//清空遗留数据
                        for (i = 0; i <= dt.Tables[0].Columns.Count - 1; i++)
                        {
                            strValue.Append(dr[i].ToString().Replace(",", ";"));
                            strValue.Append(",");
                        }
                        strValue.Remove(strValue.Length - 1, 1);//移除掉最后一个,字符
                        sw.WriteLine(strValue);
                    }
                    sw.Close();
                    MessageBox.Show("打印完成");
                }
                catch (Exception ex)
                {
                    throw new Exception($"实体导出到Csv出错：{ex.Message}");
                }
            }
        }

        public void DataToCsv2(System.Data.DataSet dt, string strName)
        {
            string strPath = strName;
            //if (File.Exists(strPath))
            //{
            //    File.Delete(strPath);
            //}

            StringBuilder strColu = new StringBuilder();
            StringBuilder strValue = new StringBuilder();
            int i = 0;
            try
            {
                StreamWriter sw = new StreamWriter(new FileStream(strPath, FileMode.CreateNew), Encoding.GetEncoding("GB2312"));
                //先打印表头
                for (i = 0; i <= dt.Tables[0].Columns.Count - 1; i++)
                {
                    strColu.Append(dt.Tables[0].Columns[i].ColumnName);
                    strColu.Append(",");
                }
                strColu.Remove(strColu.Length - 1, 1);//移除掉最后一个,字符
                sw.WriteLine(strColu);
                //再逐行打印行数据
                foreach (DataRow dr in dt.Tables[0].Rows)
                {
                    strValue.Remove(0, strValue.Length);//清空遗留数据
                    for (i = 0; i <= dt.Tables[0].Columns.Count - 1; i++)
                    {
                        strValue.Append(dr[i].ToString().Replace(",", ";"));
                        strValue.Append(",");
                    }
                    strValue.Remove(strValue.Length - 1, 1);//移除掉最后一个,字符
                    sw.WriteLine(strValue);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                throw new Exception($"实体导出到Csv出错：{ex.Message}");
            }
        }
    }
 
}
