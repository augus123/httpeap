﻿using System;
using System.IO;

namespace ToolClass
{
    public  class CsLogFun
    {
        public static object locker = new object();
        /// <summary>
        /// 生成错误信息记录
        /// </summary>
        /// <param name="err"></param>
        public static void WriteErrToLog(Exception err)
        {
            try
            {
               
                string path = Directory.GetCurrentDirectory() + @"\errlog\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\errlog\"))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\errlog\");
                }
                lock (locker)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("报错时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss") + "，内容：" + err.Message.ToString());
                            //sw.WriteLine("出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }

        }
        /// <summary>
        /// 生成接收记录
        /// </summary>
        /// <param name="err"></param>
        public static void WriteToLog(string err)
        {
            try
            {
               
                string path =  Directory.GetCurrentDirectory() + @"\HttpResult\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";//日志路径
                if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\HttpResult\"))
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\HttpResult\");
                }
                lock (locker)
                {
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            sw.WriteLine("执行时间:" + DateTime.Now.ToString("yyyy-MM-dd HH：mm：ss")+"，内容："+err.ToString());
                            //sw.WriteLine("出错文件：" + "原因：" + err.ToString());
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
                throw new Exception("提示：" + ex.Message);
            }

        }
       
    }
}
