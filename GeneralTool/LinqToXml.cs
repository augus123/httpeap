﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ToolClass
{
   public class LinqToXml
    {
        public  string[] Sx(string strPath ,string jdlj,string zjdname)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(strPath);
            List<string> st = new List<string>();//属性集合
            XmlNodeList list = doc.SelectNodes(jdlj);//最终节点路径
            foreach (XmlNode node in list)
            {
                XmlAttribute attribute = node.Attributes[zjdname];
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value.Trim()))
                {
                    st.Add(attribute.Value.Trim());
                }
            }
            return st.ToArray();
        }
        public  string[] Zjd(string strPath, string jdlj, string zjdname,int ABC)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(strPath);
            List<string> st1 = new List<string>();//子节点值集合
            XmlNodeList list = doc.SelectNodes(jdlj);//最终节点路径

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Attributes[zjdname].Value == (i + ABC).ToString())
                {
                    st1.Add(list[i].InnerText);
                }
            }
            return st1.ToArray();
        }
      
    }
}
