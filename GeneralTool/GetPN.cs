﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolClass
{
    public class GetPN
    {
        bool _P = false;
        bool _N= false;

        public bool P(bool Value)
        {
            if (Value && !_P)
            {
                _P = true;
                return true;
            }

            if (!Value)
                _P = false;

            return false;
        }

        public bool N(bool Value)
        {
            if (!Value && _N)
            {
                _N = false;
                return true;
            }

            if (Value)
                _N = true;
            return false;
        }

    }
}
