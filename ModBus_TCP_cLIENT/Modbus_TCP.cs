﻿using GeneralTool;
using Microsoft.VisualBasic;
using Modbus.Device;
using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using ToolClass;

namespace ModBus_TCP_cLIENT
{

    public class Modbus_TCP
    {
        public TcpClient tcpClient;
        public ModbusIpMaster master;
        [DllImport("WININET", CharSet = CharSet.Auto)]
        static extern bool InternetGetConnectedState(ref InternetConnectionState lpdwFlags, int dwReserved);
        enum InternetConnectionState : int
        {
            INTERNET_CONNECTION_MODEM = 0x1,
            INTERNET_CONNECTION_LAN = 0x2,
            INTERNET_CONNECTION_PROXY = 0x4,
            INTERNET_RAS_INSTALLED = 0x10,
            INTERNET_CONNECTION_OFFLINE = 0x20,
            INTERNET_CONNECTION_CONFIGURED = 0x40
        }
        #region ///MODBUS连接PLC
        /// <summary>
        /// 开始一个对远程主机的Modbus_TCP的异步请求
        /// </summary>
        /// <param name="ipAddress">主机名(IP)</param>
        /// <param name="tcpPort">端口号(post)</param>
        /// <param name="form">显示连接状态的窗体</param>
        /// <returns></returns>
        public bool Connect(string ipAddress, int tcpPort)
        {

            try
            {
                //if (master != null)
                //    master.Dispose();
                //if (tcpClient != null)
                //    tcpClient.Close();

                //if (CheckInternet())
                //{
                    try
                    {
                        tcpClient = new TcpClient();
                        IAsyncResult asyncResult = tcpClient.BeginConnect(ipAddress, tcpPort, null, null);
                        asyncResult.AsyncWaitHandle.WaitOne(3000, true);
                        if (!asyncResult.IsCompleted)
                        {
                            tcpClient.Close();
                            // MessageBox.Show(DateTime.Now.ToString() + ":无法连接到服务器.");
                            return false;
                        }
                        else {
                            master = ModbusIpMaster.CreateIp(tcpClient);
                            master.Transport.Retries = 0;
                            master.Transport.ReadTimeout = 1500;

                        

                            //   MessageBox.Show(DateTime.Now.ToString() + ":连接到服务器.");
                            return true;
                        }
                       
                    }
                    catch (Exception ex)
                    {
                    CsLogFun.WriteErrToLog(ex);
                    //  MessageBox.Show(DateTime.Now.ToString() + ":Connect process " + ex.StackTrace + "==>" + ex.Message);
                    return false;
                    }
                //}
                //else
                //{
                //    if (form.InvokeRequired)
                //    {
                //        form.Invoke(new Action(() => { form.Text = "没有Internet连接，或者所有可能的Internet连接当前都未激活"; })); 
                //    }
                //    else { form.Text = "没有Internet连接，或者所有可能的Internet连接当前都未激活"; }
                    
                //}
            }
            catch (Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
                //MessageBox.Show(ex.Message);
            }
            return false;


        }//打开TCP连接
        public bool CheckInternet()
        {
            //http://msdn.microsoft.com/en-us/library/windows/desktop/aa384702(v=vs.85).aspx
            InternetConnectionState flag = InternetConnectionState.INTERNET_CONNECTION_LAN;
            return InternetGetConnectedState(ref flag, 0);
        }

        #endregion
        /// <summary>
        /// 写入线圈地址
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">线圈对应的MODBUS地址</param>
        /// <param name="bool_">写入的状态</param>
        public void WriteSingleCoil_(byte slaveID, ushort date, bool bool_)
        {
            master.WriteSingleCoil(slaveID, date, bool_);
        }
        /// <summary>
        /// 写入寄存器地址
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">寄存器MODBUS地址</param>
        /// <param name="date_">写入的数值</param>
        public void WriteSingleRegister_(byte slaveID, ushort date, ushort date_)
        {
            master.WriteSingleRegister(slaveID, date, date_);
        }
        /// <summary>
        /// 读取输入寄存器
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">开始读取输入寄存器对应的MODBUS起始地址</param>
        /// <param name="date_">读取的长度</param>
        /// <returns>返回USHORT类型的一维数组</returns>
        public ushort[] ReadInputRegisters_(byte slaveID, ushort date, ushort date_)
        {
            ushort[] vs = new ushort[date_];
            vs = master.ReadInputRegisters(slaveID, date, date_);
            return vs;
        }
        /// <summary>
        /// 读取保持寄存器
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">开始读取保持寄存器对应的MODBUS起始地址</param>
        /// <param name="date_">读取的长度</param>
        /// <returns>返回USHORT类型的一维数组</returns>
        public ushort[] ReadHoldingRegisters_(byte slaveID, ushort date, ushort date_)
        {
            ushort[] vs = new ushort[date_];
            vs = master.ReadHoldingRegisters(slaveID, date, date_);
            return vs;
        }
        /// <summary>
        /// 读取线圈
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">开始读取线圈对应的MODBUS起始位址</param>
        /// <param name="date_">读取的长度</param>
        /// <returns>返回BOOL类型的一维数组</returns>
        public bool[] ReadCoils_(byte slaveID, ushort date, ushort date_)
        {
            bool[] vs = new bool[date_];
             vs = master.ReadCoils(slaveID, date, date_);
            return vs;
        }
        /// <summary>
        /// 读取输入
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">开始读取输入对应的MODBUS起始位址</param>
        /// <param name="date_">读取的长度</param>
        /// <returns>返回BOOL类型的一维数组</returns>
        public bool[] Readinputs_(byte slaveID, ushort date, ushort date_)
        {
            bool[] vs = new bool[date_];
            vs = master.ReadInputs(slaveID, date, date_);
            return vs;
        }
        /// <summary>
        /// 提供一个输入对话框，将输入的无符号双字节数据写入对应MODBUS地址
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">需要写入的MODBUS地址</param>
        /// <param name="max">输入最大值</param>
        /// <param name="mix">输入最小值</param>
        public void Show_WriteSingleRegister_D(string user_, byte slaveID, ushort date,int max,int mix,string b)
        {
            if (user_ == "GX" || user_ == "工程师")
            {
                try
                {
                    string a = Interaction.InputBox("请输入整数 最大:" + max + " 最小:" + mix, "GX_AVI", b);
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (Information.IsNumeric(a))
                        {
                            ushort aa = ushort.Parse(a);
                            if (aa <= max && aa >= mix)
                            {
                                master.WriteSingleRegister(slaveID, date, aa);
                            }
                            else
                            {
                                Interaction.MsgBox("超出范围,请重新输入\r\n最大值：'" + max.ToString() + "'\r\n最小值：'" + mix.ToString() + "'");
                            }
                        }
                        else
                        {
                            Interaction.MsgBox("非法字符！");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CsLogFun.WriteErrToLog(ex);
                  //  MessageBox.Show(ex.Message);
                   
                }
               
            }
        }
        /// <summary>
        /// 提供一个输入对话框，将输入的四字节整形数据写入对应MODBUS地址
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">需要写入的MODBUS起始地址</param>
        public void Show_WriteSingleRegister_DD(string user_, byte slaveID, ushort date, int max, int mix, string C)
        {
            if (user_ == "GX" || user_ == "工程师")
            {
                try
                {
                    string a = Interaction.InputBox("请输入整数:" + new String(' ', 5) + mix + "-->>" + max, "GX_AVI", C);
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (Information.IsNumeric(a))
                        {
                            int aa = int.Parse(a);
                            if (aa <= max && aa >= mix)
                            {
                                ushort[] uintData = new ushort[2];
                                //Convert Long value to ushort array                             
                                long[] longData;
                                longData = new long[1] { aa };
                                Buffer.BlockCopy(longData, 0, uintData, 0, 4);
                                for (int index = 0; index < uintData.Length; index++)
                                {
                                    ushort b = (ushort)(date + ushort.Parse(index.ToString()));
                                    master.WriteSingleRegister(slaveID, b, uintData[index]);
                                }

                            }
                            else
                            {
                                Interaction.MsgBox("超出范围,请重新输入");
                            }
                        }
                        else
                        {
                            Interaction.MsgBox("非法字符！");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CsLogFun.WriteErrToLog(ex);
                    //MessageBox.Show(ex.Message);
                  
                }
                
            }
            else
            {
                Interaction.MsgBox("当前用户登录权限限制！");
            }
        }
        public void WriteFloat(byte slaveID, ushort date, string a, float max, float mix)
        {
            if (Information.IsNumeric(a))
            {
                float ee = float.Parse(a);
                if (ee <= max && ee >= mix)
                {
                    ushort[] uintData = new ushort[2];
                    float[] longData;
                    longData = new float[1] { ee };
                    Buffer.BlockCopy(longData, 0, uintData, 0, 4);
                    for (int index = 0; index < uintData.Length; index++)
                    {
                        ushort b = (ushort)(date + ushort.Parse(index.ToString()));
                        master.WriteSingleRegister(slaveID, b, uintData[index]);
                    }
                }
                else
                {
                    Interaction.MsgBox("超出范围,请重新输入");
                }
            }
            else
            {
                Interaction.MsgBox("非法字符！");
            }
        }
        /// <summary>
        /// 提供一个输入对话框，将输入的单精度浮点数据写入对应MODBUS地址
        /// </summary>
        /// <param name="slaveID">从站号</param>
        /// <param name="date">需要写入的MODBUS起始地址</param>
        public void Show_WriteFloat_DD(string user_, byte slaveID, ushort date, float  max, float mix, string C)
        {
            if (user_ == "GX" || user_ == "工程师")
            {
                try
                {
                    string a = Interaction.InputBox("请输入小数:" + new String(' ', 5) + mix + "-->>" + max, "GX_AVI", C);
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (Information.IsNumeric(a))
                        {
                            float ee = float.Parse(a);
                            if (ee <= max && ee >= mix)
                            {
                                ushort[] uintData = new ushort[2];
                                float[] longData;
                                longData = new float[1] { ee };
                                Buffer.BlockCopy(longData, 0, uintData, 0, 4);
                                for (int index = 0; index < uintData.Length; index++)
                                {
                                    ushort b = (ushort)(date + ushort.Parse(index.ToString()));
                                    master.WriteSingleRegister(slaveID, b, uintData[index]);
                                }
                            }
                            else
                            {
                                Interaction.MsgBox("超出范围,请重新输入");
                            }
                        }
                        else
                        {
                            Interaction.MsgBox("非法字符！");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CsLogFun.WriteErrToLog(ex);
                  //  MessageBox.Show(ex.Message);
                    
                }

            }
            else
            {
                Interaction.MsgBox("当前用户登录权限限制！");
            }
        }

        public void Show_WriteFloat_DD(byte slaveID, ushort date,string a)
        {
           
                try
                {
                   
                    if (!string.IsNullOrEmpty(a))
                    {
                        if (Information.IsNumeric(a))
                        {
                            float ee = float.Parse(a);
                            
                                ushort[] uintData = new ushort[2];
                                float[] longData;
                                longData = new float[1] { ee };
                                Buffer.BlockCopy(longData, 0, uintData, 0, 4);
                                for (int index = 0; index < uintData.Length; index++)
                                {
                                    ushort b = (ushort)(date + ushort.Parse(index.ToString()));
                                    master.WriteSingleRegister(slaveID, b, uintData[index]);
                                }
                           
                        }
                        else
                        {
                            Interaction.MsgBox("非法字符！");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CsLogFun.WriteErrToLog(ex);
                   // MessageBox.Show(ex.Message);

                }

            }
           
        }
    }
