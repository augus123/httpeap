﻿using System;

namespace ModBus_TCP_cLIENT
{
    public static class ConvertDataType
    {
        /// <summary>
        /// 将两个ushort数据转换为INT32
        /// </summary>
        /// <param name="a">第一个数据</param>
        /// <param name="b">第二个数据</param>
        /// <returns>返回一个INT32类型的数据</returns>
        public static long US_IN32(ushort a,ushort b )
        {
            ushort[] data = new ushort[2];
            data[0] = b;
            data[1] = a;
            long longValue;
            longValue = (Int32)(((UInt32)data[0] << 16) | (UInt32)data[1]);
            return longValue;
        }
        /// <summary>
        /// 将一个ushort数据转换为INT16
        /// </summary>
        /// <param name="a">第一个数据</param>
        /// <returns>返回一个INT16类型的数据</returns>
        public static int US_IN16(ushort a)
        {
            ushort ushortValue = a;
            Int16 int16Value = (Int16)ushortValue;
            return int16Value;
        }
        public static string US_Float(ushort a, ushort b)
        {
            string e = "";
            ushort[] data = new ushort[2];
            data[0] =a;
            data[1] = b;
            float[] floatData = new float[data.Length / 2];
            Buffer.BlockCopy(data, 0, floatData, 0, data.Length * 2);
            for (int index = 0; index < floatData.Length; index += 2)
            {
              e=  floatData[index / 2].ToString("0.0000"); //123.4560
            }
            return e;
        }
    }
}
