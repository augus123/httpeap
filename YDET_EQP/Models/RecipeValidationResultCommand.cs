﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YDET_EQP.Models.RecipeValidationResultCommand
{

    public class RecipeValidationResultCommand
    {
        public Header Header { get; set; }=new Header();
        public Body Body { get; set; }=new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "";
        public string TransactionID { get; set; } = "";
        public string UserID { get; set; } = "";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = "";
        public string RecipeID { get; set; } = "";
        public int Action { get; set; }
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = ""; 
        public string MessageEN { get; set; } = "";
    }

}
