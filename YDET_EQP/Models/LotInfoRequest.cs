﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Properties;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: $description$
 * 
 */
namespace YDET_EQP.Models.LotInfoRequest
{
    public class LotInfoRequest
    {
        public Header Header { get; set; } = new Header();
        public Body Body { get; set; } = new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "LotInfoRequest";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
        public string UserID { get; set; } = "";
        public string RequestID { get; set; } = "";
        public string FeebarID { get; set; } = "";
        public int RequestType { get; set; }
        public int PortStatus { get; set; }
        public List<Panellist> PanelList { get; set; } = new List<Panellist>();
      
    }

    public class Panellist
    {
        public string TrayID { get; set; } = "";
        public string PanelID { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = "";
    }

}
