﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Properties;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: $description$
 * 
 */
namespace YDET_EQP.Models.MaterialStatusReport
{
    public class MaterialStatusReport
    {
        public Header Header { get; set; } = new Header();
        public Body Body { get; set; } = new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "MaterialStatusReport";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
        public string UnitID { get; set; } = "";

        public string MaterialID { get; set; } = "";
        public string MaterialType { get; set; } = "";
        public string MaterialQTY { get; set; } = "";
        public string MaterialStatus { get; set; } = "";
        public string Position { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; }   = "";               
        public string MessageEN { get; set; } = "";
    }

}
