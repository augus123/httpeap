﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Properties;
/**
* 
* 作者; LEGION
* 时间: 2023/11/23 17:02:14
* 描述: 设备向 EAP 请求时间，设备自动请求时间的触发点在设备切换 CIM OFF 到CIM ON 时设备主动请求，
* 手动请求是在设备人机界面上添加时间请求按/钮，人工点击按钮请求
* 
*/
namespace YDET_EQP.Models.EQPDateTimeRequest
{
    public class EQPDateTimeRequest
    {
        public Header Header { get; set; }=new Header();
        public Body Body { get; set; }=new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "EQPDateTimeRequest";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
        public string DateTime { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; }= 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = ""; 
    }
}
