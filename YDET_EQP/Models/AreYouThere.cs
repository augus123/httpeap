﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Properties;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: ：设备程序启动时，主动发
送心跳事件，间隔4s上报一次，
在10s内（时间可设置）设备监
控不到EAP心跳时，设备报警
 * 
 */
namespace YDET_EQP.Models.AreYouThere
{
    public class AreYouThere
    {
        public Header Header { get; set; }= new Header();
        public Body Body { get; set; }=new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "AreYouThere";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = "";
    }

}
