﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Properties;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: $description$
 * 
 */
namespace YDET_EQP.Models.ToolStatusReport
{
    public class ToolStatusReport
    {
        public Header Header { get; set; } = new Header();
        public Body Body { get; set; } = new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "ToolStatusReport";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
        public string UnitID { get; set; } = "";
        public string ToolID { get; set; } = "";
        public string ToolStatus { get; set; } = "";
        public string Position { get; set; } = "";
        public string ToolQTY { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = "";
    }

}
