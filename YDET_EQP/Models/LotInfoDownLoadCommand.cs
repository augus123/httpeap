﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.Models.EQPDataCollectionReport;
using YDET_EQP.Properties;

namespace YDET_EQP.Models.LotInfoDownloadCommand
{
    public class LotInfoDownloadCommand
    {
        public Header Header { get; set; }=new Header();
        public Body Body { get; set; }=new Body();
        public Result Result { get; set; } = new Result();
    }

    public class Header
    {
        public string MessageName { get; set; } = "LotInfoDownloadCommand";
        public string TransactionID { get; set; } = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
        public string UserID { get; set; } = "01";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = Settings.Default.设备ID;
        public string PortID { get; set; } = "";
        public string LotID { get; set; } = "";
        public string CarrierID { get; set; } = "";
        public string ProductNO { get; set; } = "";
        public string PlaceInterval { get; set; } = "";
        public string RecipeID { get; set; } = "";
        public List<Recipelist> RecipeList { get; set; } = new List<Recipelist>();
        public string PanelQTY { get; set; } = "";
        public string SamplingQTY { get; set; } = "";
        public List<Panellist> PanelList { get; set; } = new List<Panellist>();
    }

    public class Recipelist
    {
        public string Name { get; set; } = "";
        public string Value { get; set; } = "";
    }

    public class Panellist
    {
        public string PanelID { get; set; } = "";
        public List<Setidlist> SetIDList { get; set; } = new List<Setidlist>(); 
        public string Value { get; set; } = "";
    }

    public class Setidlist
    {
        public string SetID { get; set; } = "";
        public string SetIndex { get; set; } = "";
        public string SetGrade { get; set; } = "";
        public List<Pcsidlist> PCSIDList { get; set; } = new List<Pcsidlist>();
    }

    public class Pcsidlist
    {
        public string PCSID { get; set; } = "";
        public string PCSIndex { get; set; } = "";
        public string PCSGrade { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; }
        public string MessageCH { get; set; } = "";     
        public string MessageEN { get; set; } = "";
    }

}
