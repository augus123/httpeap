﻿using System.Collections.Generic;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: $description$
 * 
 */
namespace YDET_EQP.Models.EQPPMCodeReport
{
    public class EQPPMCodeReport
    {
        public Header Header { get; set; }
        public Body Body { get; set; }
        public Result Result { get; set; }
    }

    public class Header
    {
        public string MessageName { get; set; } = "";
        public string TransactionID { get; set; } = "";
        public string UserID { get; set; } = "";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = "";
        public  List<PMCode> PMCode { get; set; }=new List<PMCode>();
    }

    public class PMCode
    {
        public string Name { get; set; } = "";
        public string Value { get; set; } = "";
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = "";
    }
}
