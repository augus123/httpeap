﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 * 
 * 作者; LEGION
 * 时间: 2023/11/23 17:02:14
 * 描述: $description$
 * 
 */
namespace YDET_EQP.Models.EQPSendOutJobReport
{
    public class EQPSendOutJobReport
    {
        public Header Header { get; set; }
        public Body Body { get; set; }
        public Result Result { get; set; }
    }

    public class Header
    {
        public string MessageName { get; set; } = "";
        public string TransactionID { get; set; } = "";
        public string UserID { get; set; } = "";
    }

    public class Body
    {
        public string EquipmentID { get; set; } = "";
        public string Type1PanelID { get; set; } = "";
        public string Type2PanelID { get; set; } = "";
        public string Type3PanelID { get; set; } = "";
        public int SequenceNO { get; set; }
        public int LotID { get; set; }
    }

    public class Result
    {
        public int Code { get; set; } = 1;
        public string MessageCH { get; set; } = "";
        public string MessageEN { get; set; } = "";
    }
}
