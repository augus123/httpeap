﻿using ModBus_TCP_cLIENT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YDET_EQP.ViewModels;
using YDET_EQP.Views;
using static YDET_EQP.ViewModels.ListMessage_Alarm;

namespace YDET_EQP
{
    public static class CsPublicVariablies
    {
        public static bool Eap_h;
        public static bool Eap_run;
        public static long sj_run,sj_sys;
        public static string Eap_url;

        public static User_model _data1 = new User_model();
        //TCP通信
        public static bool IsPLCclient=false;
        public static Modbus_TCP modbus_TCP = new Modbus_TCP();
        public static bool[] vm0 = new bool[2000];//m0-m1499;
        public static ushort[] vhd1100=new ushort[100];
        public static ushort[] vhd550 = new ushort[100];
        public static bool[] vhm700 = new bool[100];
        public static string[] Alert_grade = new string[110];
        public static string[] Alert_txt = new string[110];
        public static string[] Alert_num = new string[110];
        //报警消息动态集合
        public static ObservableCollection<ListMessage_Alarm> List_MSG_Alarm = new ObservableCollection<ListMessage_Alarm>();


        public static ObservableCollection<ListMessage_Action> List_MSG_Action = new ObservableCollection<ListMessage_Action>();

        public static ObservableCollection<Message_EapModel>  message_EapModels = new ObservableCollection<Message_EapModel>();

        public static Tool_materialModel tool_MaterialModel = new Tool_materialModel();
        public static Produce_taskModel produce_taskModel = new Produce_taskModel();

        public static Message_Eap message_Eap=new Message_Eap() ;
    }
}
