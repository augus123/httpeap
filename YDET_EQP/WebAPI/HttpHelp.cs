﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GeneralTool;
using GeneralTool.Properties;
using YDET_EQP.Properties;
using System.Xml.Linq;
using Newtonsoft.Json;
using YDET_EQP.Models.AreYouThere;
using ToolClass;

namespace HTTP_Demo
{
    public class HttpHelp
    {
         public static  byte[] b = Encoding.Default.GetBytes(Settings.Default.设备ID);
        /// <summary>
        /// GET
        /// </summary>
        /// <param name="url">请求的url</param>
        public static string Get(string url)
        {
            HttpWebRequest hwRequest = null;
            HttpWebResponse hwResponse = null;

            string strResult = string.Empty;
            try
            {
                hwRequest = (System.Net.HttpWebRequest)WebRequest.Create(url);
                hwRequest.Method = "GET";
                hwRequest.Timeout = 2000;
                hwRequest.ContentType = "application/x-www-form-urlencoded";
            }
            catch (System.Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
            }
            try
            {
                hwResponse = (HttpWebResponse)hwRequest.GetResponse();

                StreamReader srReader = new StreamReader(hwResponse.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8);
               
                strResult = srReader.ReadToEnd();
                srReader.Close();
                hwResponse.Close();
            }
            catch (System.Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
            }
            return strResult;
        }
        /// <summary> 
        /// post
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static string Post(object classname, string url,  string contentType)
        {
                string result = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "restApi/" + classname.GetType().Name);
                request.ContentType = contentType;
                request.Method = "POST";
                request.Timeout = Settings.Default.http超时时间;
                request.Headers.Add("Basic_base64_auth_string", Convert.ToBase64String(HttpHelp.b));
                request.Headers.Add("EqpId", Settings.Default.设备ID);
                try
                {
                    string json1 = JsonConvert.SerializeObject(classname);
                    byte[] bytes = Encoding.UTF8.GetBytes(json1);
                    request.ContentLength = bytes.Length;
                    Stream writer = request.GetRequestStream();
                    writer.Write(bytes, 0, bytes.Length);
                    writer.Close();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8);
                    result = reader.ReadToEnd();
                    response.Close();
                }
                catch (Exception ex)
                {
                    CsLogFun.WriteErrToLog(ex);
                }
                return result;
        }
        /// <summary> 
        /// postAsync
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public static async Task<string> PostAsync(object classname, string url, string contentType)
        {
            string result = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "restApi/" + classname.GetType().Name);
            request.ContentType = contentType;
            request.Method = "POST";
            request.Timeout = Settings.Default.http超时时间;
            request.Headers.Add("Basic_base64_auth_string", Convert.ToBase64String(b));
            request.Headers.Add("EqpId", Settings.Default.设备ID);
            try
            {
                string json1 = JsonConvert.SerializeObject(classname);
                byte[] bytes = Encoding.UTF8.GetBytes(json1);
                request.ContentLength = bytes.Length;
                Stream writer = await request.GetRequestStreamAsync();
                writer.Write(bytes, 0, bytes.Length);
                writer.Close();

                HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse;
                StreamReader reader = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8);
                result = await reader.ReadToEndAsync();
                response.Close();
            }
            catch (Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
            }
            return result;
        }
        public static string HttpPostRaw(string url, string data)
        {
            string value = "";
            string error = "";
            try
            {
                HttpWebRequest reqest = (HttpWebRequest)WebRequest.Create(url);
                reqest.Method = "POST";
                reqest.ContentType = "application/json";
                Stream stream = reqest.GetRequestStream();
                byte[] bs = System.Text.Encoding.UTF8.GetBytes(data);
                stream.Write(bs, 0, bs.Length);
                stream.Flush();
                stream.Close();
                HttpWebResponse response = (HttpWebResponse)reqest.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                value = sr.ReadToEnd();
                response.Close();
                return value;
            }
            catch (Exception ex)
            {
                CsLogFun.WriteErrToLog(ex);
                return "";
            }
        }
        #region Json字符处理
        public static string ConvertJsonString(string str)
        {
            //格式化json字符串
            JsonSerializer serializer = new JsonSerializer();
            TextReader tr = new StringReader(str);
            JsonTextReader jtr = new JsonTextReader(tr);
            object obj = serializer.Deserialize(jtr);
            if (obj != null)
            {
                StringWriter textWriter = new StringWriter();
                JsonTextWriter jsonWriter = new JsonTextWriter(textWriter)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4,
                    IndentChar = ' '
                };
                serializer.Serialize(jsonWriter, obj);
                return textWriter.ToString();
            }
            else
            {
                return str;
            }
        }
        #endregion
    }
}
