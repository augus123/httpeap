﻿using HTTP_Demo;
using HTTPServerLib;
using Newtonsoft.Json;
using System;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Navigation;
using ToolClass;
using YDET_EQP.Models.CIMMessageCommand;
using YDET_EQP.Models.DateTimeCommand;
using YDET_EQP.Models.EQPRecipeReport;
using YDET_EQP.Models.EQPStatusChangeReport;
using YDET_EQP.Models.LotInfoDownloadCommand;
using YDET_EQP.Models.MaterialValidationRequestReply;
using YDET_EQP.Models.RecipeValidationResultCommand;
using YDET_EQP.Models.ToolValidationRequestReply;
using YDET_EQP.Properties;
using YDET_EQP.ViewModels;
using YDET_EQP.Views;
using static YDET_EQP.ViewModels.ListMessage_Alarm;

namespace YDET_EQP.WebAPI
{
    public class ExampleServer : HTTPServerLib.HttpServer
    {

        private void show_msg(string str)
        {
            App.Current.Dispatcher.Invoke((Action)(() =>
            {
                CsPublicVariablies.message_EapModels.Insert(0, new Message_EapModel()
                {
                    Time_EAPAlarm = DateTime.Now.ToString(),
                    Message_EAPAlarm = str
                });
             
                    CsPublicVariablies.message_Eap.Show();
               
            }));
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ipAddress">IP地址</param>
        /// <param name="port">端口号</param>
        public ExampleServer(Window window_, string ipAddress, int port)
            : base(ipAddress, port)
        {
            window=window_;
        }
        Window window = new Window();  
        int i = 0;
        public override void OnPost(HttpRequest request, HttpResponse response)
        {
         

           
            if (request.URL == "/restApi/AreYouThereReply")
            {
                //AreYouThereReply.Body body= new AreYouThereReply.Body();
                //body = (AreYouThereReply.Body)Newtonsoft.Json.JsonConvert.DeserializeObject<AreYouThereReply.Body>(request.Body);

                SendMsg_Action("EAP向本机发送心跳");
                CsPublicVariablies.Eap_h = true;
                //设置返回信息
                //string result = Newtonsoft.Json.JsonConvert.SerializeObject(request.Body);
                string result =request.Body;
                //构造响应报文
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";

                //发送响应
                 response.Send();
            }
            if (request.URL == "/restApi/DateTimeCommand")
            {
                MessageBox.Show(request.URL);
                SendMsg_Action("EAP向本机发送系统时间");
                DateTimeCommand dateTimeCommand = new DateTimeCommand();
                dateTimeCommand = (DateTimeCommand)Newtonsoft.Json.JsonConvert.DeserializeObject<DateTimeCommand>(request.Body);
                var dateTime = DateTime.ParseExact(dateTimeCommand.Body.DateTime, "yyyyMMddHHmmss", System.Globalization.CultureInfo.CurrentCulture);
                SYSTEMTIME st = new SYSTEMTIME();
                st.FromDateTime(dateTime);
                //调用Win32 API设置系统时间
                Win32API.SetLocalTime(ref st);

                //设置返回信息
                string result = request.Body;
                //构造响应报文0
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";
                //发送响应
                response.Send();

            }
            if(request.URL == "/restApi/CIMMessageCommand")
            {
                MessageBox.Show(request.URL);
                CIMMessageCommand cIMMessageCommand  = new CIMMessageCommand();
                cIMMessageCommand = (CIMMessageCommand)Newtonsoft.Json.JsonConvert.DeserializeObject<CIMMessageCommand>(request.Body);
                CsLogFun.WriteToLog(cIMMessageCommand.Body.Message);

             
                SendMsg_Action("EAP向本机发送信息:" + cIMMessageCommand.Body.Message);
                show_msg(cIMMessageCommand.Body.Message);

                CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1,7,true);

                //设置返回信息
                string result = request.Body;
                //构造响应报文0
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";
                //发送响应
                response.Send();
            }
            if (request.URL == "/restApi/MaterialValidationRequestReply")
            {
                MessageBox.Show(request.URL);
                MaterialValidationRequestReply materialValidationRequestReply  = new MaterialValidationRequestReply();
                materialValidationRequestReply = (MaterialValidationRequestReply)Newtonsoft.Json.JsonConvert.DeserializeObject<MaterialValidationRequestReply>(request.Body);
                if (materialValidationRequestReply.Body.ResultCode == "1")
                {
                    SendMsg_Action("EAP向本机下发物料信息");
                    App.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        CsPublicVariablies.tool_MaterialModel.Material_number = materialValidationRequestReply.Body.MaterialQTY;
                        CsPublicVariablies.tool_MaterialModel.Material_id=materialValidationRequestReply.Body.MaterialID;
                        CsPublicVariablies.tool_MaterialModel.Material_location = "M1";
                        //MessageBox.Show(materialValidationRequestReply.Body.MaterialQTY);
                        CsPublicVariablies.modbus_TCP.WriteFloat(1, 42196,materialValidationRequestReply.Body.MaterialQTY, (float)100000.00, (float)0.00);//hd1108
                   
                        ShowPrintAlm showPrintAlm = new ShowPrintAlm();
                        showPrintAlm.benStart.Text = "EAP向本机下发物料信息成功";
                        showPrintAlm.Show();
                    }));
                }
               


                //设置返回信息
                string result = request.Body;
                //构造响应报文0
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";
                //发送响应
                response.Send();
            
            }

            if (request.URL == "/restApi/ToolValidationRequestReply")
            {
                MessageBox.Show(request.URL);
                ToolValidationRequestReply toolValidationRequestReply  = new ToolValidationRequestReply();
                toolValidationRequestReply = (ToolValidationRequestReply)Newtonsoft.Json.JsonConvert.DeserializeObject<ToolValidationRequestReply>(request.Body);
                if (toolValidationRequestReply.Body.ResultCode == "1")
                {
                    SendMsg_Action("EAP向本机下发工具信息");
                    App.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        CsPublicVariablies.tool_MaterialModel.Tool_number = toolValidationRequestReply.Body.ToolQTY;
                        CsPublicVariablies.tool_MaterialModel.Tool_id= toolValidationRequestReply.Body.ToolID;
                        CsPublicVariablies.tool_MaterialModel.Tool_id = "T1";
                        CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 42193, ushort.Parse(toolValidationRequestReply.Body.ToolQTY));//hd1105
                        ShowPrintAlm showPrintAlm = new ShowPrintAlm();
                        showPrintAlm.benStart.Text = "EAP向本机下发工具信息成功";
                        showPrintAlm.Show();
                    }));
                }

                 //设置返回信息
                string result = request.Body;
                //构造响应报文0
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";
                //发送响应
                response.Send();
              
            }

            if (request.URL == "/restApi/LotInfoDownloadCommand")
            {
                MessageBox.Show(request.URL);
                    try
                    {
                        SendMsg_Action("EAP向本机下发生产任务信息");
                        LotInfoDownloadCommand lotInfoDownLoadCommand_rest = new LotInfoDownloadCommand();

                        LotInfoDownloadCommand lotInfoDownLoadCommand = new LotInfoDownloadCommand();
                        lotInfoDownLoadCommand = (LotInfoDownloadCommand)Newtonsoft.Json.JsonConvert.DeserializeObject<LotInfoDownloadCommand>(request.Body);
                    App.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        CsPublicVariablies.produce_taskModel.Formula_id_ready = lotInfoDownLoadCommand.Body.RecipeID;
                        CsPublicVariablies.produce_taskModel.Formula_type_ready = lotInfoDownLoadCommand.Body.ProductNO;
                        CsPublicVariablies.produce_taskModel.Formula_batch_ready = lotInfoDownLoadCommand.Body.LotID;
                        CsPublicVariablies.produce_taskModel.Formula_number_ready = int.Parse(lotInfoDownLoadCommand.Body.PanelQTY);


                        CsPublicVariablies.produce_taskModel.Print_Speed = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "PrintingSpeed").Value;
                        CsPublicVariablies.produce_taskModel.Back_Speed = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "BackInkSpeed").Value;
                        CsPublicVariablies.produce_taskModel.Print_number = int.Parse(lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "PrintingFrequency").Value);
                        CsPublicVariablies.produce_taskModel.Double_Blade = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "DoubleBlade").Value;
                        CsPublicVariablies.produce_taskModel.AfterClean = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "AfterClean").Value;
                        CsPublicVariablies.produce_taskModel.BeforeClean = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "BeforeClean").Value;
                        CsPublicVariablies.produce_taskModel.StopLeft = lotInfoDownLoadCommand.Body.RecipeList.Find(p => p.Name == "StopLeft").Value;
                        lotInfoDownLoadCommand_rest.Body = lotInfoDownLoadCommand.Body;

                        if (lotInfoDownLoadCommand.Body.RecipeList.Count > 7)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "配方下发的参数数量多了";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.RecipeList.Count < 7)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "配方下发的参数数量少了";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.RecipeList.Count == 0 || lotInfoDownLoadCommand.Body.RecipeList == null)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "配方下发的参数为空";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.RecipeID == "" || lotInfoDownLoadCommand.Body.RecipeID == null)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "配方下发的配方ID不存在";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (int.Parse(CsPublicVariablies.produce_taskModel.Print_Speed) > 2000 || int.Parse(CsPublicVariablies.produce_taskModel.Print_Speed) < 0
                            || int.Parse(CsPublicVariablies.produce_taskModel.Back_Speed) > 2000 || int.Parse(CsPublicVariablies.produce_taskModel.Back_Speed) < 0
                            || CsPublicVariablies.produce_taskModel.Print_number > 10 || CsPublicVariablies.produce_taskModel.Print_number < 0)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "配方下发的配方参数值超范围";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);

                        }
                        else if (lotInfoDownLoadCommand.Body.LotID == "")
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "LotID为空";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.ProductNO == "")
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "ProductNO为空";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.PanelQTY == "")
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "PanelQTY为空";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (lotInfoDownLoadCommand.Body.RecipeID == "")
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "RecipeID为空";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (Settings.Default.连接模式 == "CIM Off")
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "CIM OFF离线模式";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else if (CsPublicVariablies.Eap_run)
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 0;
                            lotInfoDownLoadCommand_rest.Result.MessageCH = "  设备已有验证通过任务，未完工收到新任务";
                            show_msg(lotInfoDownLoadCommand_rest.Result.MessageCH);
                        }
                        else
                        {
                            lotInfoDownLoadCommand_rest.Result.Code = 1;

                        }
                    }));
                   





                    
                        //设置返回信息
                        string result = JsonConvert.SerializeObject(lotInfoDownLoadCommand_rest);
                        //构造响应报文0
                        response.SetContent(result);
                        response.Content_Encoding = "utf-8";
                        response.StatusCode = "200";
                        response.Content_Type = "application/json";
                        response.Headers["Server"] = "ExampleServer";
                        //发送响应
                        response.Send();

                        if (lotInfoDownLoadCommand_rest.Result.Code == 1)
                        {
                            EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                            eQPRecipeReport.Body.RecipeID = lotInfoDownLoadCommand_rest.Body.RecipeID;
                            eQPRecipeReport.Body.Action = 4;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                            eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                            eQPRecipeReport.Body.Result = 1;
                            HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                        }
                    }
                    catch (Exception ex) {
                        MessageBox.Show(ex.Message);
                    }
            }

            if (request.URL == "/restApi/RecipeValidationResultCommand")
            {
                MessageBox.Show(request.URL);
                RecipeValidationResultCommand recipeValidationResultCommand  = new RecipeValidationResultCommand();
                recipeValidationResultCommand = (RecipeValidationResultCommand)Newtonsoft.Json.JsonConvert.DeserializeObject<RecipeValidationResultCommand>(request.Body);
                if (recipeValidationResultCommand.Result.Code == 1)
                {
                    string str;
                    switch (recipeValidationResultCommand.Body.Action)
                    {
                        case 1:
                            str = "新增配方回复";
                            break;
                        case 2:
                            str = "新增配方回复";
                            break;
                        case 3:
                            str = "删除配方回复";
                            break;
                        case 4:
                            str = "验证配方回复";
                            break;
                        case 5:
                            str = "请求配方回复";
                            break;
                        default:
                            str = "无回复";
                            break;
                    }
                    App.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        CsPublicVariablies.produce_taskModel.Formula_id_run = CsPublicVariablies.produce_taskModel.Formula_id_ready;
                    CsPublicVariablies.produce_taskModel.Formula_type_run = CsPublicVariablies.produce_taskModel.Formula_type_ready;
                    CsPublicVariablies.produce_taskModel.Formula_batch_run = CsPublicVariablies.produce_taskModel.Formula_batch_ready;
                    CsPublicVariablies.produce_taskModel.Formula_number_run = CsPublicVariablies.produce_taskModel.Formula_number_ready;
     
                    CsPublicVariablies.modbus_TCP.WriteFloat(1, 42210, CsPublicVariablies.produce_taskModel.Print_Speed, (float)1000.00, (float)100.00);
                    CsPublicVariablies.modbus_TCP.WriteFloat(1, 42214, CsPublicVariablies.produce_taskModel.Back_Speed, (float)1000.00, (float)100.00);
                    CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 41839, (ushort)CsPublicVariablies.produce_taskModel.Print_number);
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50122, Convert.ToBoolean(CsPublicVariablies.produce_taskModel.Double_Blade));
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50125, Convert.ToBoolean(CsPublicVariablies.produce_taskModel.AfterClean));
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50109, Convert.ToBoolean(CsPublicVariablies.produce_taskModel.BeforeClean));
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50119, Convert.ToBoolean(CsPublicVariablies.produce_taskModel.StopLeft));
                    SendMsg_Action("EAP向本机"+ str);
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 6, true);
                   // CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 42192, (ushort)CsPublicVariablies.produce_taskModel.Formula_number_run);
                    CsPublicVariablies.Eap_run=true;
                    
                        ShowPrintAlm showPrintAlm = new ShowPrintAlm();
                        showPrintAlm.benStart.Text = "EAP允许丝印机开始生产";
                        showPrintAlm.Show();
                    }));
                }

                //设置返回信息
                string result = request.Body;
                //构造响应报文0
                response.SetContent(result);
                response.Content_Encoding = "utf-8";
                response.StatusCode = "200";
                response.Content_Type = "application/json";
                response.Headers["Server"] = "ExampleServer";
                //发送响应
                response.Send();
            }
        
        }

        private void SendMsg_Action(string str)
        {
            window.Dispatcher.Invoke((Action)(() =>
            {
                CsPublicVariablies.List_MSG_Action.Insert(0, new ListMessage_Action()
                {
                    Time_Action = DateTime.Now.ToString(),
                    Message_Action = str
                });
            }));
            if (CsPublicVariablies.List_MSG_Action.Count > 35)
            {
                window.Dispatcher.Invoke((Action)(() =>
                {
                    CsPublicVariablies.List_MSG_Action.RemoveAt(35);
                }));
            }
        }

        public long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalMilliseconds);
        }
        public override void OnGet(HttpRequest request, HttpResponse response)
        {

            ///链接形式1:"http://localhost:4050/assets/styles/style.css"表示访问指定文件资源，
            ///此时读取服务器目录下的/assets/styles/style.css文件。

            ///链接形式1:"http://localhost:4050/assets/styles/"表示访问指定页面资源，
            ///此时读取服务器目录下的/assets/styles/style.index文件。

            //当文件不存在时应返回404状态码
            string requestURL = request.URL;
            requestURL = requestURL.Replace("/", @"\").Replace("\\..", "").TrimStart('\\');
            string requestFile = Path.Combine(ServerRoot, requestURL);

            //判断地址中是否存在扩展名
            string extension = Path.GetExtension(requestFile);

            //根据有无扩展名按照两种不同链接进行处
            if (extension != "")
            {
                //从文件中返回HTTP响应
                response = response.FromFile(requestFile);
            }
            else
            {
                //目录存在且不存在index页面时时列举目录
                if (Directory.Exists(requestFile) && !File.Exists(requestFile + "\\index.html"))
                {
                    requestFile = Path.Combine(ServerRoot, requestFile);
                    var content = ListDirectory(requestFile, requestURL);
                    response = response.SetContent(content, Encoding.UTF8);
                    response.Content_Type = "text/html; charset=UTF-8";
                }
                else
                {
                    //加载静态HTML页面
                    requestFile = Path.Combine(requestFile, "index.html");
                    response = response.FromFile(requestFile);
                    response.Content_Type = "text/html; charset=UTF-8";
                }
            }

            //发送HTTP响应
            response.Send();
        }

        public override void OnDefault(HttpRequest request, HttpResponse response)
        {

        }

        private string ConvertPath(string[] urls)
        {
            string html = string.Empty;
            int length = ServerRoot.Length;
            foreach (var url in urls)
            {
                var s = url.StartsWith("..") ? url : url.Substring(length).TrimEnd('\\');
                html += String.Format("<li><a href=\"{0}\">{0}</a></li>", s);
            }

            return html;
        }

        private string ListDirectory(string requestDirectory, string requestURL)
        {
            //列举子目录
            var folders = requestURL.Length > 1 ? new string[] { "../" } : new string[] { };
            folders = folders.Concat(Directory.GetDirectories(requestDirectory)).ToArray();
            var foldersList = ConvertPath(folders);

            //列举文件
            var files = Directory.GetFiles(requestDirectory);
            var filesList = ConvertPath(files);

            //构造HTML
            StringBuilder builder = new StringBuilder();
            builder.Append(string.Format("<html><head><title>{0}</title></head>", requestDirectory));
            builder.Append(string.Format("<body><h1>{0}</h1><br/><ul>{1}{2}</ul></body></html>",
                 requestURL, filesList, foldersList));

            return builder.ToString();
        }
    }
}
