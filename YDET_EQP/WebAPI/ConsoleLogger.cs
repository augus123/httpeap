﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTTPServerLib;
using ToolClass;

namespace HttpServer
{
    public class ConsoleLogger:ILogger
    {
        public void Log(object message)
        {
            if (message.GetType() == typeof(Exception))
            {
                CsLogFun.WriteErrToLog((Exception)message);
            }
            else
            {
                CsLogFun.WriteToLog(message.ToString());
            }
        }
    }
}
