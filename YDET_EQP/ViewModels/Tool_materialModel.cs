﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HTTP_Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using YDET_EQP.Models.EQPCompletedReport;
using YDET_EQP.Models.MaterialStatusReport;
using YDET_EQP.Models.MaterialUseReport;
using YDET_EQP.Models.MaterialValidationRequest;
using YDET_EQP.Models.ToolStatusReport;
using YDET_EQP.Models.ToolUseReport;
using YDET_EQP.Models.ToolValidationRequest;
using YDET_EQP.Properties;

namespace YDET_EQP.ViewModels
{
    public class Tool_materialModel: ObservableObject
    {
        public ICommand Tool_load { get; set; }
        public ICommand Tool_unload { get; set; }
        public ICommand Material_load { get; set; }
        public ICommand Material_unload { get; set; }
        public Tool_materialModel()
        {
            //物料上机请求
            Material_load = new RelayCommand(() => {
                if (User_id==""|| Material_id == ""|| User_id == null || Material_id == null)
                {
                    MessageBox.Show("请先输入用户ID和物料ID");
                    return;
                }
                MaterialValidationRequest materialValidationRequest = new MaterialValidationRequest();
                materialValidationRequest.Body.UserID = User_id;
                materialValidationRequest.Body.MaterialID = Material_id;
                materialValidationRequest.Body.Position = Material_location;
                HttpHelp.Post(materialValidationRequest,CsPublicVariablies.Eap_url, "application/json");
            });

            //物料下机请求，是否需要判断是否处于任务执行中
            Material_unload = new RelayCommand(() => {
                if (User_id == "" || Material_id == "" || User_id == null || Material_id == null)
                {
                    MessageBox.Show("请先输入用户ID和物料ID");
                    return;
                }
                MaterialStatusReport materialStatusReport  = new MaterialStatusReport();
                materialStatusReport.Body.MaterialStatus = "2";
                materialStatusReport.Body.MaterialID = Material_id;
                materialStatusReport.Body.MaterialQTY = CsPublicVariablies.vhd1100[9].ToString();
                HttpHelp.Post(materialStatusReport, CsPublicVariablies.Eap_url, "application/json");
                material_id = "";
                material_number = "";
            });

            //工具上机请求
            Tool_load = new RelayCommand(() => {
                if (User_id == "" || Tool_id == "" || User_id == null || Tool_id == null)
                {
                    MessageBox.Show("请先输入用户ID和工具ID");
                    return;
                }
                ToolValidationRequest toolValidationRequest  = new ToolValidationRequest();
                toolValidationRequest.Body.UserID = User_id;
                toolValidationRequest.Body.ToolID = Tool_id;
                toolValidationRequest.Body.Position = Tool_location;
                HttpHelp.Post(toolValidationRequest, CsPublicVariablies.Eap_url, "application/json");
            });

            //工具下机请求
            Tool_unload = new RelayCommand(() => {
                if (User_id == "" || Tool_id == "" || User_id == null || Tool_id == null)
                {
                    MessageBox.Show("请先输入用户ID和工具ID");
                    return;
                }
                ToolStatusReport toolStatusReport  = new ToolStatusReport();
                toolStatusReport.Body.ToolStatus = "2";
                toolStatusReport.Body.ToolID = Tool_id;
                toolStatusReport.Body.ToolQTY = CsPublicVariablies.vhd1100[5].ToString();
                HttpHelp.Post(toolStatusReport, CsPublicVariablies.Eap_url, "application/json");
                tool_id = "";
                tool_number = "";
            });
        }

        private string user_id = Settings.Default.操作员ID;
        public string User_id
        {
            get { return user_id; }
            set { Settings.Default.操作员ID = value; Settings.Default.Save(); SetProperty(ref user_id, value); }
        }

        private string tool_id = Settings.Default.工具ID;
        public string Tool_id
        {
            get { return tool_id; }
            set { Settings.Default.工具ID = value; Settings.Default.Save(); SetProperty(ref tool_id, value); }
        }

      
      

        private string tool_number;
        public string Tool_number
        {
            get => tool_number;
            set => SetProperty(ref tool_number, value);
        }

        private string tool_location="T1";
        public string Tool_location
        {
            get => tool_location;
            set => SetProperty(ref tool_location, value);
        }

        private string material_id = Settings.Default.物料ID;
        public string Material_id
        {
            get { return material_id; }
            set { Settings.Default.物料ID = value; Settings.Default.Save(); SetProperty(ref material_id, value); }
        }


        private string material_number = Settings.Default.物料数量;
        public string Material_number
        {
            get { return material_number; }
            set { Settings.Default.物料数量 = value; Settings.Default.Save(); SetProperty(ref material_number, value); }
        }

        

        private string material_location = "M1";
        public string Material_location
        {
            get => material_location;
            set => SetProperty(ref material_location, value);
        }


    }
}
