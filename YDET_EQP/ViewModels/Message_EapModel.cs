﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YDET_EQP.ViewModels
{
    public class Message_EapModel : ObservableObject
    {
        private string time_EAPAlarm;
        private string message_EAPAlarm;

        public string Time_EAPAlarm
        {
            get => time_EAPAlarm;
            set => SetProperty(ref time_EAPAlarm, value);
        }
        public string Message_EAPAlarm
        {
            get { return message_EAPAlarm; }
            set
            {
                SetProperty(ref message_EAPAlarm, value);
            }
        }
    }
}
