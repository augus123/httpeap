﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using YDET_EQP.Properties;

namespace YDET_EQP.ViewModels
{
    public class HmiViewModel : ObservableObject
    {

        public HmiViewModel()
        {
            Timeout_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                int b = Timeout;
                string a = Interaction.InputBox("请输入超时时间(ms)", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    Timeout = int.Parse(a);
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            EAP_rate_set = new RelayCommand(() =>
            {
                int b = EAP_rate;
                string a = Interaction.InputBox("请输入数据采集频率", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    EAP_rate = int.Parse(a);
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            EAP_ip_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string b = EAP_ip;
                string a = Interaction.InputBox("请输入服务器IP地址", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    EAP_ip = a;
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            EAP_port_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string b = EAP_port;
                string a = Interaction.InputBox("请输入服务器端口", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    EAP_port = a;
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            EAP_heartbeat_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                int b = EAP_heartbeat;
                string a = Interaction.InputBox("请输入心跳时间", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    EAP_heartbeat = int.Parse(a);
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Equipment_id_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string b = Equipment_id;
                string a = Interaction.InputBox("请输入设备端ID", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    Equipment_id = a;
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Equipment_ip_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string b = Equipment_ip;
                string a = Interaction.InputBox("请输入设备端IP地址", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    Equipment_ip = a;
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Equipment_port_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string b = Equipment_port;
                string a = Interaction.InputBox("请输入设备端口", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    Equipment_port = a;
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Equipment_heartbeat_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                int b = Equipment_heartbeat;
                string a = Interaction.InputBox("请输入心跳时间", "GX", b.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    Equipment_heartbeat = int.Parse(a);
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });
        }

        private int timeout = Settings.Default.http超时时间;
        public int Timeout
        {
            get { return timeout; }
            set { Settings.Default.http超时时间 = value; Settings.Default.Save(); SetProperty(ref timeout, value); }
        }
        public ICommand Timeout_set { get; set; }

        private string userName;
        public string UserName
        {
            get => userName;
            set => SetProperty(ref userName, value);
        }

        private string sysTime;
        public string SysTime
        {
            get => sysTime;
            set => SetProperty(ref sysTime, value);
        }

        private string nowTime;
        public string NowTime
        {
            get { return nowTime; }
            set
            {
                SetProperty(ref nowTime, value);
            }
        }

        private string eAP_state;
        public string EAP_state
        {
            get { return eAP_state; }
            set { SetProperty(ref eAP_state, value); }
        }

        private string eAP_heart="10";
        public string EAP_heart
        {
            get { return eAP_heart; }
            set { SetProperty(ref eAP_heart, value); }
        }

        private string eQP_state;
        public string EQP_state
        {
            get { return eQP_state; }
            set { SetProperty(ref eQP_state, value); }
        }

        private int eAP_rate = Settings.Default.数据采集频率;
        public int EAP_rate
        {
            get { return eAP_rate; }
            set { Settings.Default.数据采集频率 = value; Settings.Default.Save(); SetProperty(ref eAP_rate, value); }
        }
        public ICommand EAP_rate_set { get; set; }

        private string eQP_runtime;
        public string EQP_runtime
        {
            get { return eQP_runtime; }
            set { SetProperty(ref eQP_runtime, value); }
        }

        private Brush communicationStatus;
        public Brush CommunicationStatus
        {
            get { return communicationStatus; }
            set { SetProperty(ref communicationStatus, value); }
        }

        private string communicationStatusTxt = Settings.Default.连接模式;
        public string CommunicationStatusTxt
        {
            get { return communicationStatusTxt; }
            set { Settings.Default.连接模式 = value; Settings.Default.Save(); SetProperty(ref communicationStatusTxt, value); }
        }


        private string eAP_ip = Settings.Default.EAP服务器IP;
        public string EAP_ip
        {
            get { return eAP_ip; }
            set { Settings.Default.EAP服务器IP = value; Settings.Default.Save(); SetProperty(ref eAP_ip, value); }
        }
        public ICommand EAP_ip_set { get; set; }

        private string eAP_port = Settings.Default.EAP服务器端口;
        public string EAP_port
        {
            get { return eAP_port; }
            set { Settings.Default.EAP服务器端口 = value; Settings.Default.Save(); SetProperty(ref eAP_port, value); }
        }
        public ICommand EAP_port_set { get; set; }

        private int eAP_heartbeat = Settings.Default.EAP心跳时间;
        public int EAP_heartbeat
        {
            get { return eAP_heartbeat; }
            set { Settings.Default.EAP心跳时间 = value; Settings.Default.Save(); SetProperty(ref eAP_heartbeat, value); }
        }
        public ICommand EAP_heartbeat_set { get; set; }

        private string equipment_id = Settings.Default.设备ID;
        public string Equipment_id
        {
            get { return equipment_id; }
            set { Settings.Default.设备ID = value; Settings.Default.Save(); SetProperty(ref equipment_id, value); }
        }
        public ICommand Equipment_id_set { get; set; }

        private string equipment_ip = Settings.Default.设备端IP;
        public string Equipment_ip
        {
            get { return equipment_ip; }
            set { Settings.Default.设备端IP = value; Settings.Default.Save(); SetProperty(ref equipment_ip, value); }
        }
        public ICommand Equipment_ip_set { get; set; }

        private string equipment_port = Settings.Default.设备端端口;
        public string Equipment_port
        {
            get { return equipment_port; }
            set { Settings.Default.设备端端口 = value; Settings.Default.Save(); SetProperty(ref equipment_port, value); }
        }
        public ICommand Equipment_port_set { get; set; }

        private int equipment_heartbeat = Settings.Default.设备端心跳时间;
        public int Equipment_heartbeat
        {
            get { return equipment_heartbeat; }
            set { Settings.Default.设备端心跳时间 = value; Settings.Default.Save(); SetProperty(ref equipment_heartbeat, value); }
        }
        public ICommand Equipment_heartbeat_set { get; set; }




        private string run_State;
        public string Run_State
        {
            get { return run_State; }
            set { SetProperty(ref run_State, value); }
        }

        private string wait_State;
        public string Wait_State
        {
            get { return wait_State; }
            set { SetProperty(ref wait_State, value); }
        }

        private string idle_State;
        public string Idle_State
        {
            get { return idle_State; }
            set { SetProperty(ref idle_State, value); }
        }

        private string upkeep_State;
        public string Upkeep_State
        {
            get { return upkeep_State; }
            set { SetProperty(ref upkeep_State, value); }
        }
    }

    #region--------报警列表
    public class ListMessage_Alarm : ObservableObject
    {
        private string time_Alarm;
        private string message_Alarm;

        public string Time_Alarm
        {
            get => time_Alarm;
            set => SetProperty(ref time_Alarm, value);
        }
        public string Message_Alarm
        {
            get { return message_Alarm; }
            set
            {
                SetProperty(ref message_Alarm, value);
            }
        }
        #endregion

        #region--------列表
        public class ListMessage_Action : ObservableObject
        {
            private string time_Action;
            private string message_Action;

            public string Time_Action
            {
                get => time_Action;
                set => SetProperty(ref time_Action, value);
            }
            public string Message_Action
            {
                get { return message_Action; }
                set
                {
                    SetProperty(ref message_Action, value);
                }
            }
            #endregion
        }
    }
}
