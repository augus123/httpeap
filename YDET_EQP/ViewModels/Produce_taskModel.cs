﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HTTP_Demo;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using YDET_EQP.Models.EQPRecipeReport;
using YDET_EQP.Models.LotInfoRequest;
using YDET_EQP.Properties;

namespace YDET_EQP.ViewModels
{
    public class Produce_taskModel : ObservableObject
    {
        public Produce_taskModel()
        {

            Task_post = new RelayCommand(() =>
                {
                    if (Settings.Default.连接模式 == "CIM On")
                    {
                        try
                        {
                            // 设备请求生产 Lot 信息
                            LotInfoRequest lotInfoRequest = new LotInfoRequest();
                            lotInfoRequest.Body.UserID = CsPublicVariablies.tool_MaterialModel.User_id;
                            //lotInfoRequest.Body.RequestType = LOT.SelectedIndex + 1;
                            lotInfoRequest.Body.RequestType =  1;
                            lotInfoRequest.Body.RequestID = PC_id;
                            lotInfoRequest.Body.PortStatus = 3;
                            HttpHelp.Post(lotInfoRequest, CsPublicVariablies.Eap_url, "application/json");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                });
            Print_Speed_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("请输入印刷速度", "GX", Print_Speed_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteFloat(1, 42210, a, (float)1000.00, (float)100.00);
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Back_Speed_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("请输入回墨速度", "GX", Back_Speed_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteFloat(1, 42214, a, (float)1000.00, (float)100.00);
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Print_number_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("请输入印刷次数", "GX", Print_number_now.ToString());
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 41839, ushort.Parse(a));
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            Double_Blade_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("是否打开双刮刀模式  \"TRUE / FALSE\"", "GX", Print_Speed_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50122, Convert.ToBoolean(a));
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            StopLeft_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("是否停右  \"TRUE / FALSE\"", "GX", StopLeft_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50119, Convert.ToBoolean(a));
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            AfterClean_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("是否开启印刷前清洁  \"TRUE / FALSE\"", "GX", AfterClean_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50125, Convert.ToBoolean(a));
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });

            BeforeClean_now_set = new RelayCommand(() =>
            {
                if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
                {
                    MessageBox.Show("请先登录Admin");
                    return;
                }
                string a = Interaction.InputBox("是否开启印刷后清洁  \"TRUE / FALSE\"", "GX", BeforeClean_now);
                if (!string.IsNullOrEmpty(a))
                {
                    CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 50109, Convert.ToBoolean(a));
                    EQPRecipeReport eQPRecipeReport = new EQPRecipeReport();
                    eQPRecipeReport.Body.Action = 5;//1：新增配方回复 2：修改配方回 3：删除配方回复4：验证配方回复5：设备修改配方参数值时上报
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "PrintingFrequency", Value = (CsPublicVariablies.produce_taskModel.Print_number.ToString()) });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "DoubleBlade", Value = CsPublicVariablies.produce_taskModel.Double_Blade });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.BeforeClean });
                    eQPRecipeReport.Body.RecipeList.Add(new RecipeList() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.StopLeft });
                    eQPRecipeReport.Body.Result = 1;
                    HttpHelp.Post(eQPRecipeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                else
                {
                    // Interaction.MsgBox("非法字符！");
                }
            });
        }
        private string zb_numb;
        public string Zb_numb

        {
            get => zb_numb;
            set => SetProperty(ref zb_numb, value);
        }

        private string pC_id;
        public string PC_id

        {
            get => pC_id;
            set => SetProperty(ref pC_id, value);
        }

        private string formula_id_run;
        public string Formula_id_run
        {
            get => formula_id_run;
            set => SetProperty(ref formula_id_run, value);
        }

        private string formula_type_run;
        public string Formula_type_run
        {
            get => formula_type_run;
            set => SetProperty(ref formula_type_run, value);
        }

        private string formula_batch_run;
        public string Formula_batch_run
        {
            get => formula_batch_run;
            set => SetProperty(ref formula_batch_run, value);
        }

        private int formula_number_run;
        public int Formula_number_run
        {
            get => formula_number_run;
            set => SetProperty(ref formula_number_run, value);
        }

        private string formula_id_ready;
        public string Formula_id_ready
        {
            get => formula_id_ready;
            set => SetProperty(ref formula_id_ready, value);
        }

        private string formula_type_ready;
        public string Formula_type_ready
        {
            get => formula_type_ready;
            set => SetProperty(ref formula_type_ready, value);
        }

        private string formula_batch_ready;
        public string Formula_batch_ready
        {
            get => formula_batch_ready;
            set => SetProperty(ref formula_batch_ready, value);
        }

        private int formula_number_ready;
        public int Formula_number_ready
        {
            get => formula_number_ready;
            set => SetProperty(ref formula_number_ready, value);
        }

        public ICommand Print_Speed_now_set { get; set; }

        public ICommand Task_post { get; set; }
        private string print_Speed_now;
        public string Print_Speed_now
        {
            get => print_Speed_now;
            set => SetProperty(ref print_Speed_now, value);
        }

        public ICommand Back_Speed_now_set { get; set; }
        private string back_Speed_now;
        public string Back_Speed_now
        {
            get => back_Speed_now;
            set => SetProperty(ref back_Speed_now, value);
        }

        public ICommand Print_number_now_set { get; set; }
        private int print_number_now;
        public int Print_number_now
        {
            get => print_number_now;
            set => SetProperty(ref print_number_now, value);
        }

        public ICommand Double_Blade_now_set { get; set; }
        private string double_Blade_now;
        public string Double_Blade_now
        {
            get => double_Blade_now;
            set => SetProperty(ref double_Blade_now, value);
        }

        public ICommand StopLeft_now_set { get; set; }
        private string stopLeft_now;
        public string StopLeft_now
        {
            get => stopLeft_now;
            set => SetProperty(ref stopLeft_now, value);
        }

        public ICommand AfterClean_now_set { get; set; }
        private string afterClean_now;
        public string AfterClean_now
        {
            get => afterClean_now;
            set => SetProperty(ref afterClean_now, value);
        }

        public ICommand BeforeClean_now_set { get; set; }
        private string beforeClean_now;
        public string BeforeClean_now
        {
            get => beforeClean_now;
            set => SetProperty(ref beforeClean_now, value);
        }

        private string print_Speed;
        public string Print_Speed
        {
            get => print_Speed;
            set => SetProperty(ref print_Speed, value);
        }

        private string back_Speed;
        public string Back_Speed
        {
            get => back_Speed;
            set => SetProperty(ref back_Speed, value);
        }

        private int print_number;
        public int Print_number
        {
            get => print_number;
            set => SetProperty(ref print_number, value);
        }

        private string double_Blade;
        public string Double_Blade
        {
            get => double_Blade;
            set => SetProperty(ref double_Blade, value);
        }

        private string stopLeft;
        public string StopLeft
        {
            get => stopLeft;
            set => SetProperty(ref stopLeft, value);
        }

        private string afterClean;
        public string AfterClean
        {
            get => afterClean;
            set => SetProperty(ref afterClean, value);
        }

        private string beforeClean;
        public string BeforeClean
        {
            get => beforeClean;
            set => SetProperty(ref beforeClean, value);
        }
    }
}
