﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YDET_EQP.Properties;
using YDET_EQP.ViewModels;

namespace YDET_EQP.Views
{
    /// <summary>
    /// Tool_material.xaml 的交互逻辑
    /// </summary>
    public partial class Tool_material : Window
    {
        public Tool_material(Tool_materialModel tool_MaterialModel)
        {
            InitializeComponent();
            this.DataContext = tool_MaterialModel;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Task.Run(() => UI_show());//读取plc状态
        }

        private void UI_show()
        {
            while (true)
            {
                Thread.Sleep(100);
                this.Dispatcher.Invoke(() => {
                    CsPublicVariablies.tool_MaterialModel.Tool_number = CsPublicVariablies.vhd1100[5].ToString();
                });
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();

        }
    }
}
