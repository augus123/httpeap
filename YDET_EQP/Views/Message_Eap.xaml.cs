﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace YDET_EQP.Views
{
    /// <summary>
    /// Message_Eap.xaml 的交互逻辑
    /// </summary>
    public partial class Message_Eap : Window
    {
        public Message_Eap()
        {
            InitializeComponent();
            Mesg_Alarm.ItemsSource = CsPublicVariablies.message_EapModels;
        }
        
        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
