﻿using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows;
using ToolClass;

namespace YDET_EQP.Views
{
    /// <summary>
    /// Login_interface.xaml 的交互逻辑
    /// </summary>
    public partial class Login_interface : Window
    {
        public Login_interface()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //如果BLL层中 useLogin调用返回记录条数 大于1 则账号密码正确
                string strSql = "select * from 用户表 where user = '" + use.Text + "'and password = '" + Pa.Password + "'";
                if (SQLiteHelper.GetSingleResult(strSql) != null)
                {
                    CsPublicVariablies._data1.user = use.Text;
                    CsPublicVariablies._data1.password = Pa.Password;
                    WeakReferenceMessenger.Default.Send<User_model, string>(CsPublicVariablies._data1, "User_model_token");
                    this.DialogResult = Convert.ToBoolean(1);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("登录失败");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = Convert.ToBoolean(0);
        }
        List<UserList> list = new List<UserList>();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataSet dt = new DataSet();
            dt = SQLiteHelper.GetDataSet("SELECT * FROM 用户表");
            if (dt != null)
            {
                int i = 1;
                foreach (DataRow row in dt.Tables[0].Rows)
                {
                    list.Add(new UserList { id = i, Name = row.ItemArray[0].ToString() });
                    i++;
                }
            }

            use.ItemsSource = list;

        }
    }
}
