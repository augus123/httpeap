﻿using CommunityToolkit.Mvvm.Messaging;
using HTTP_Demo;
using HttpServer;
using Microsoft.SqlServer.Server;
using ModBus_TCP_cLIENT;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using ToolClass;
using YDET_EQP.Models.AreYouThere;
using YDET_EQP.Models.CIMMessageCommand;
using YDET_EQP.Models.EQPAlarmReport;
using YDET_EQP.Models.EQPCommunicationStatusReport;
using YDET_EQP.Models.EQPCompletedReport;
using YDET_EQP.Models.EQPDataCollectionReport;
using YDET_EQP.Models.EQPDateTimeRequest;
using YDET_EQP.Models.EQPStatusChangeReport;
using YDET_EQP.Models.LotInfoRequest;
using YDET_EQP.Models.MaterialUseReport;
using YDET_EQP.Models.ToolUseReport;
using YDET_EQP.Properties;
using YDET_EQP.ViewModels;
using YDET_EQP.Views;
using YDET_EQP.WebAPI;

namespace YDET_EQP
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HmiView : Window
    {
      //  public static HmiView hmiView;
        private DateTime _startdate = DateTime.Now;
        HmiViewModel hmiViewModel=new HmiViewModel();
    
        Tool_material tool_Material = new Tool_material(CsPublicVariablies.tool_MaterialModel);
      
        Produce_task produce_Task = new Produce_task(CsPublicVariablies.produce_taskModel);
        BarcodeScannerHelper barcodeScannerHelper = new BarcodeScannerHelper();
        public HmiView()
        {
            InitializeComponent();
            Mesg_Alarm.ItemsSource = CsPublicVariablies.List_MSG_Alarm;
            Mesg_Action.ItemsSource = CsPublicVariablies.List_MSG_Action;
            hmi.DataContext = hmiViewModel;
            tool_Material.Hide();
            produce_Task.Hide();
            //hmiView=this;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("你确定要退出程序吗?", "操作提示", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.OK) == MessageBoxResult.OK)
            {
                barcodeScannerHelper.Stop();
                Settings.Default.运行时间 = CsPublicVariablies.sj_run;
                Settings.Default.总开机时间 = CsPublicVariablies.sj_sys;
                Settings.Default.Save();
                System.Environment.Exit(0);
            }  
            else
            {
                e.Cancel = true;
            }
        }
       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CsPublicVariablies.sj_run = Settings.Default.运行时间;
            CsPublicVariablies.sj_sys = Settings.Default.总开机时间;
            ExampleServer server = new ExampleServer(this, Settings.Default.设备端IP, int.Parse(Settings.Default.设备端端口));
            server.SetRoot(@"D:\Hexo\public");
            server.Logger = new ConsoleLogger();
            Task.Run(() => server.Start());
            

            WeakReferenceMessenger.Default.Register<User_model, string>(this, "User_model_token", t_user);
            #region 创建数据集

            SQLiteHelper.ConStr = "Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "YDET_EQP.db";
           
            if (!SQLiteHelper.ExistTable("用户表", AppDomain.CurrentDomain.BaseDirectory + "YDET_EQP.db"))
            {
                SQLiteHelper.NewTable("YDET_EQP.db", "CREATE TABLE 用户表 (user STRING,password STRING)");
                SQLiteHelper.Update("INSERT INTO 用户表(user,password) VALUES('Admin','087853')");
                SQLiteHelper.Update("INSERT INTO 用户表(user,password) VALUES('工程师','123')");
                SQLiteHelper.Update("INSERT INTO 用户表(user,password) VALUES('操作员','456')");
            }
            #endregion 创建数据集


           CsPublicVariablies.Eap_url = "http://" + Settings.Default.EAP服务器IP + ":" + Settings.Default.EAP服务器端口+"/";
            if (Settings.Default.连接模式 == "CIM On")
            {
                hmiViewModel.EQP_state = "3";
                CommunicationStatusReport(1);
                EQPDateTimeRequest eQPDateTimeRequest = new EQPDateTimeRequest();
                HttpHelp.Post(eQPDateTimeRequest, CsPublicVariablies.Eap_url, "application/json");
            }
            else { hmiViewModel.EQP_state = "2"; CommunicationStatusReport(2); }
            LinqToXml linqToXml = new LinqToXml();
            int i = 0;
            string[] strings = linqToXml.Zjd(AppDomain.CurrentDomain.BaseDirectory + "\\Alert_messages.xml", @"AlertS/Alert", "ID", 300);
            foreach (var item in strings)
            {
                string[] sArray = item.Split(new char[1] { ',' });
                CsPublicVariablies.Alert_grade[i] = sArray[0];
                CsPublicVariablies.Alert_txt[i] = sArray[1];
                i++;
            }
            CsPublicVariablies.Alert_num = linqToXml.Sx(AppDomain.CurrentDomain.BaseDirectory + "\\Alert_messages.xml", @"AlertS/Alert", "ID");
            Task.Run(() => Readplc());//读取plc状态
            Task.Run(() => { Alert_in(); });//报警上报
            Task.Run(() => { BootTime(); });//系统运行时间
            Task.Run(() => { EQPDataCollectionReport(); });//系统定时采集
            Task.Run(() => { Eqp_ONOFF(); });
            Task.Run(() => { Eqp_state(); });
            Task.Run(() => { Eap_ONOFF(); });
            Task.Run(() => { Task_run(); });
            Task task = new Task(() => { RunTime(); }); //运行时间
            task.Start();
            Task task1 = new Task(() => { SysTime(); }); //运行时间
            task1.Start();



            barcodeScannerHelper.Start();
            barcodeScannerHelper.ScanerEvent += BarcodeScannerHelper_ScanerEvent;
        }

        private void BarcodeScannerHelper_ScanerEvent(string codes)
        {
            //if (Settings.Default.连接模式 == "CIM On")
            //{
            //    if (codes.Length >= 20)//判断字符串结构是否匹配工单规则
            //    {
            //        / 设备请求生产 Lot 信息
            //        LotInfoRequest lotInfoRequest = new LotInfoRequest();
            //        lotInfoRequest.Body.UserID = CsPublicVariablies.tool_MaterialModel.User_id;
            //        lotInfoRequest.Body.RequestType = 1;
            //        lotInfoRequest.Body.PortStatus = 3;
            //        HttpHelp.Post(lotInfoRequest, CsPublicVariablies.Eap_url, "application/json");
            //    }
            //}


        }

        private void t_user(object recipient, User_model message)
        {
            hmiViewModel.UserName = message.user;
        }
       
     

        private void  EQPDataCollectionReport()
        {
            while (true)
            {
                Thread.Sleep(10);
                EQPDataCollectionReport eQPDataCollectionReport = new EQPDataCollectionReport();
                eQPDataCollectionReport.Body.TotalOnTime = hmiViewModel.SysTime;
                eQPDataCollectionReport.Body.TotalProTime = hmiViewModel.EQP_runtime;
                eQPDataCollectionReport.Body.ProductNO = CsPublicVariablies.produce_taskModel.Formula_type_run;
                eQPDataCollectionReport.Body.LotID = CsPublicVariablies.produce_taskModel.Formula_batch_run;
                List<Parameterlist> strings = new List<Parameterlist>
                {
                    new Parameterlist() { Name = "PrintingSpeed", Value = CsPublicVariablies.produce_taskModel.Print_Speed_now },
                    new Parameterlist() { Name = "BackInkSpeed", Value = CsPublicVariablies.produce_taskModel.Back_Speed_now },
                    new Parameterlist() { Name = "PrintingFrequency", Value = CsPublicVariablies.produce_taskModel.Print_number_now.ToString() },
                    new Parameterlist() { Name = "DoubleBlade", Value =  CsPublicVariablies.produce_taskModel.Double_Blade_now },
                    new Parameterlist() { Name = "BeforeClean", Value = CsPublicVariablies.produce_taskModel.StopLeft_now },
                    new Parameterlist() { Name = "AfterClean", Value = CsPublicVariablies.produce_taskModel.AfterClean_now },
                    new Parameterlist() { Name = "StopLeft", Value = CsPublicVariablies.produce_taskModel.BeforeClean_now }
                };
                eQPDataCollectionReport.Body.ParameterList = strings;
                HttpHelp.Post(eQPDataCollectionReport, CsPublicVariablies.Eap_url, "application/json");
                Thread.Sleep(Settings.Default.数据采集频率 * 1000);
            }          
        }
        private  void RunTime()
        {
            while (true)
            {
                CsPublicVariablies.sj_run++;
                Thread.Sleep(1000);
                hmiViewModel.EQP_runtime = Math.Truncate((1000.00* CsPublicVariablies.sj_run / 60000.00)).ToString();
                while (!CsPublicVariablies.vm0[1200]) Thread.Sleep(10); ;
            }
            
        }

        private void SysTime()
        {
            while (true)
            {
                CsPublicVariablies.sj_sys++;
                Thread.Sleep(1000);
                hmiViewModel.SysTime = Math.Truncate((1000.00 * CsPublicVariablies.sj_sys / 60000.00)).ToString();
                while (!CsPublicVariablies.IsPLCclient) Thread.Sleep(10); ;
              
            }

        }
        List<GetPN> listPN_zt = new List<GetPN>();
        private void  Eqp_state()
        {
            for (int i = 0; i < 20; i++)
            {
                listPN_zt.Add(new GetPN());
            }
            while (true)
            {
                Thread.Sleep(200);
                //设备状态分为四种：RUN 运行/IDLE 闲置/DOWN 宕机/PM 保养，RUN 运
                //行 / IDLE 闲置 / DOWN 宕机为设备主动触发，PM 保养人机界面上需要按钮人
                //工操作，设备状态发生改变时才上报，

                if (listPN_zt[0].P(CsPublicVariablies.vm0[1200]))//运行
                {
                    EQPStatusChangeReport eQPStatusChangeReport = new EQPStatusChangeReport();
                    eQPStatusChangeReport.Body.Status = "2";
                    HttpHelp.Post(eQPStatusChangeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                if (listPN_zt[1].P(CsPublicVariablies.vm0[1201]))//闲置
                {
                    EQPStatusChangeReport eQPStatusChangeReport = new EQPStatusChangeReport();
                    eQPStatusChangeReport.Body.Status = "1";
                    HttpHelp.Post(eQPStatusChangeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                if (listPN_zt[2].P(CsPublicVariablies.vm0[1202]))//待机
                {
                    EQPStatusChangeReport eQPStatusChangeReport = new EQPStatusChangeReport();
                    eQPStatusChangeReport.Body.Status = "3";
                    HttpHelp.Post(eQPStatusChangeReport, CsPublicVariablies.Eap_url, "application/json");
                }
                if (listPN_zt[3].P(CsPublicVariablies.vm0[1203]))//保养
                {
                    EQPStatusChangeReport eQPStatusChangeReport = new EQPStatusChangeReport();
                    eQPStatusChangeReport.Body.Status = "4";
                    HttpHelp.Post(eQPStatusChangeReport, CsPublicVariablies.Eap_url, "application/json");
                }
            }
            
        }

        private void Eqp_ONOFF()
        {
            while (true)
            {
              
                AreYouThere areYouThere = new AreYouThere();
                string response =  HttpHelp.Post(areYouThere, CsPublicVariablies.Eap_url, "application/json");
                hmiViewModel.EAP_state = "1";
                Thread.Sleep(1000);
                hmiViewModel.EAP_state = "0";
                Thread.Sleep(Settings.Default.EAP心跳时间 * 1000 - 1000);
            }
        }
        private void Eap_ONOFF()
        {
            long i= 0;
            while (true)
            {
                Thread.Sleep(10);
                if (CsPublicVariablies.Eap_h)
                {
                    i = 0;
                    hmiViewModel.EAP_heart = "11";
                    Thread.Sleep(1000);
                    hmiViewModel.EAP_heart = "10";
                     CsPublicVariablies.Eap_h = false;
                }
                else
                {
                    i++;
                    Thread.Sleep(1000);
                    if (i * 1000 >= Settings.Default.设备端心跳时间 * 1000)
                    {
                        i = 0;
                        MessageBox.Show("   EAP心跳超时");
                    }
                }
            }
        }
        private void BootTime()
        {
           while (true)
            { 
                Thread.Sleep(100);
                hmiViewModel.NowTime = DateTime.Now.ToString("G");
                
              
                this.Dispatcher.Invoke(() => {
                    if (Settings.Default.连接模式 == "CIM On")
                    { 
                        hmiViewModel.CommunicationStatus = new SolidColorBrush(Colors.ForestGreen);
                        if (CsPublicVariablies.IsPLCclient)
                        {
                            CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 8, true);
                        }
                        
                    }
                    else { hmiViewModel.CommunicationStatus = new SolidColorBrush(Colors.Red); 
                        if (CsPublicVariablies.IsPLCclient)
                        {
                            CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 8, false);
                        }
                    }

                    if (CsPublicVariablies.vm0[1203])
                    {
                        upkeep.Background = new SolidColorBrush(Colors.ForestGreen);
                        hmiViewModel.Upkeep_State = "7";
                    }
                    else
                    {
                        upkeep.Background = new SolidColorBrush(Colors.White);
                        hmiViewModel.Upkeep_State = "6";
                    }
                    if (CsPublicVariablies.vm0[1200])
                    {
                        hmiViewModel.Run_State = "1";
                    }
                    else
                    {
                        hmiViewModel.Run_State = "0";
                    }
                    if (CsPublicVariablies.vm0[1202])
                    {
                        hmiViewModel.Wait_State = "3";
                    }
                    else
                    {
                        hmiViewModel.Wait_State = "2";
                    }
                    if (CsPublicVariablies.vm0[1201])
                    {
                        hmiViewModel.Idle_State = "5";
                    }
                    else
                    {
                        hmiViewModel.Idle_State = "4";
                    }

                    if (hmiViewModel.CommunicationStatusTxt == "CIM On")
                    {
                        forced_finish.IsEnabled = true;
                        open_tool.IsEnabled = true;
                        open_task.IsEnabled= true; 
                    }
                    else
                    {
                        forced_finish.IsEnabled = false;
                        open_tool.IsEnabled = false;
                        open_task.IsEnabled = false;
                    }
                });
            }
        }
        #region----------------------------------报警
        List<GetPN> listPN = new List<GetPN>();
        private  void Alert_in()
        {
           
            for (int i = 0; i < 144; i++)
            {
                listPN.Add(new GetPN());
            }
            while (true)
            {
                Thread.Sleep(100);
                Console.WriteLine("Alert_in:" + Thread.CurrentThread.ManagedThreadId.ToString()); 
                if (CsPublicVariablies.IsPLCclient )
                {
                    for (int i = 0; i < 100; i++)
                    {
                        if (listPN[i].P(CsPublicVariablies.vm0[i + 300])) 
                        {
                                this.Dispatcher.Invoke((Action)(() =>
                                {
                                    CsPublicVariablies.List_MSG_Alarm.Insert(0, new ListMessage_Alarm()
                                    {
                                        Time_Alarm = DateTime.Now.ToString(),
                                        Message_Alarm = CsPublicVariablies.Alert_txt[i]
                                    });
                                }));

                            EQPAlarmReport eQPAlarmReport = new EQPAlarmReport();
                            eQPAlarmReport.Body.AlarmID = CsPublicVariablies.Alert_num[i];
                            eQPAlarmReport.Body.AlarmLevel = int.Parse(CsPublicVariablies.Alert_grade[i]);
                            eQPAlarmReport.Body.AlarmStatus = 1;
                            eQPAlarmReport.Body.AlarmText = CsPublicVariablies.Alert_txt[i];
                            HttpHelp.Post(eQPAlarmReport, CsPublicVariablies.Eap_url, "application/json");
                        }
                        if (listPN[i].N(CsPublicVariablies.vm0[i + 300]))
                        {
                            var result = CsPublicVariablies.List_MSG_Alarm.First(p => (bool)(p.Message_Alarm == CsPublicVariablies.Alert_txt[i] ));
                            var index = CsPublicVariablies.List_MSG_Alarm.IndexOf(result);
                            if (index >= 0)
                                //move it to the top of the list
                                this.Dispatcher.Invoke((Action)(() =>
                                {
                                    CsPublicVariablies.List_MSG_Alarm.RemoveAt(index);
                                }));

                            EQPAlarmReport eQPAlarmReport = new EQPAlarmReport();
                            eQPAlarmReport.Body.AlarmID = CsPublicVariablies.Alert_num[i];
                            eQPAlarmReport.Body.AlarmLevel = int.Parse(CsPublicVariablies.Alert_grade[i]);
                            eQPAlarmReport.Body.AlarmStatus = 2;
                            eQPAlarmReport.Body.AlarmText = CsPublicVariablies.Alert_txt[i];
                            HttpHelp.Post(eQPAlarmReport, CsPublicVariablies.Eap_url, "application/json");
                        }
                    }
                }
                else { this.Dispatcher.Invoke((Action)(() => { CsPublicVariablies.List_MSG_Alarm.Clear();
                 listPN = new List<GetPN>();
                    for (int i = 0; i < 144; i++)
                    {
                        listPN.Add(new GetPN());
                    }
                })) ; }
            }
        }
        #endregion

        
        #region----------------------------------PLCread
        private void Readplc()
        {
            while (true)
            {
                Thread.Sleep(100);
                if (CsPublicVariablies.IsPLCclient)
                {
                    try
                    {
                        this.Dispatcher.Invoke(() => {
                            plcIScontxt.Text = "PLC已连接";
                            plcIScontxt.Background = Brushes.Green;
                        });
                        CsPublicVariablies.vm0 = CsPublicVariablies.modbus_TCP.ReadCoils_(1, 0, 2000);
                        CsPublicVariablies.vhd1100 = CsPublicVariablies.modbus_TCP.ReadHoldingRegisters_(1, 42188, 100);//41088<>hd0
                        CsPublicVariablies.vhm700 = CsPublicVariablies.modbus_TCP.ReadCoils_(1, 50108, 100);//49408<>hm0
                        CsPublicVariablies.vhd550 = CsPublicVariablies.modbus_TCP.ReadHoldingRegisters_(1, 41638, 100);
                        CsPublicVariablies.produce_taskModel.Print_Speed_now = ConvertDataType.US_Float(CsPublicVariablies.vhd1100[22], CsPublicVariablies.vhd1100[23]);
                        CsPublicVariablies.produce_taskModel.Back_Speed_now = ConvertDataType.US_Float(CsPublicVariablies.vhd1100[26], CsPublicVariablies.vhd1100[27]);
                        CsPublicVariablies.produce_taskModel.Print_number_now = (int)CsPublicVariablies.modbus_TCP.ReadHoldingRegisters_(1, 41839, 1)[0];
                        CsPublicVariablies.produce_taskModel.Double_Blade_now = CsPublicVariablies.vhm700[14].ToString();
                        CsPublicVariablies.produce_taskModel.StopLeft_now = CsPublicVariablies.vhm700[11].ToString();
                        CsPublicVariablies.produce_taskModel.AfterClean_now = CsPublicVariablies.vhm700[17].ToString();
                        CsPublicVariablies.produce_taskModel.BeforeClean_now = CsPublicVariablies.vhm700[1].ToString();
                    }
                    catch (Exception ex)
                    {
                        CsPublicVariablies.IsPLCclient = false;
                        this.Dispatcher.Invoke(() => {
                            plcIScontxt.Text = "PLC已断开";
                            plcIScontxt.Background = Brushes.Red;
                        });
                    }
                }
                else
                {
                    CsPublicVariablies.IsPLCclient = false;
                     this.Dispatcher.Invoke(() => { plcIScontxt.Text = "PLC正在重连...";
                         plcIScontxt.Background = Brushes.Yellow;
                     });
                    Thread.Sleep(200);
                    CsPublicVariablies.IsPLCclient =CsPublicVariablies.modbus_TCP.Connect("192.168.6.6", 502);
                }
            }
        }
        #endregion
        #region-----------------------------事件
        private async void Button_Click(object sender, RoutedEventArgs e)
        {

          

            EQPDateTimeRequest eQPDateTimeRequest = new EQPDateTimeRequest();
           await HttpHelp.PostAsync(eQPDateTimeRequest, CsPublicVariablies.Eap_url, "application/json");
        }

        private async void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CsPublicVariablies._data1.user != "Admin" || CsPublicVariablies._data1.user == null)
            {
                MessageBox.Show("请先登录Admin");
                return;
            }
            if (hmiViewModel.CommunicationStatusTxt == "CIM On")
            {
                hmiViewModel.CommunicationStatusTxt = "CIM Off";
                hmiViewModel.EQP_state = "2";
                await CommunicationStatusReport(2);
            }
            else 
            { 
                hmiViewModel.CommunicationStatusTxt = "CIM On"; 
                hmiViewModel.EQP_state = "3" ; 
                Settings.Default.Save(); 
                await CommunicationStatusReport(1);
                EQPDateTimeRequest eQPDateTimeRequest = new EQPDateTimeRequest();
                HttpHelp.Post(eQPDateTimeRequest, CsPublicVariablies.Eap_url, "application/json");
            }
        }

        private  async Task CommunicationStatusReport(int a)
        {
            EQPCommunicationStatusReport eQPCommunicationStatusReport = new EQPCommunicationStatusReport();
            eQPCommunicationStatusReport.Body.CommunicationStates = a;
            await HttpHelp.PostAsync(eQPCommunicationStatusReport,   CsPublicVariablies.Eap_url, "application/json");
        }
        #endregion

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Login_interface loginWindow = new Login_interface();
            loginWindow.ShowDialog();
            if (loginWindow.DialogResult != Convert.ToBoolean(1))
            {
            }
        }

        private void open_tool_Click(object sender, RoutedEventArgs e)
        {
            if (tool_Material.Visibility==Visibility.Hidden)
            {
                tool_Material.Show();
            }
            else
            {
                tool_Material.Hide();
            }
        }

        private void open_task_Click(object sender, RoutedEventArgs e)
        {
            if (produce_Task.Visibility == Visibility.Hidden)
            {
                produce_Task.Show();
            }
            else
            {
                produce_Task.Hide();
            }
        }
        private void Task_run()
        {
            while (true)
            {
                Thread.Sleep(100);
                CsPublicVariablies.produce_taskModel.Zb_numb = CsPublicVariablies.vhd1100[4].ToString();
                if (CsPublicVariablies.Eap_run)
                {
                    if (CsPublicVariablies.vhd1100[4]>=CsPublicVariablies.produce_taskModel.Formula_number_run)//张数大于下发任务数量 
                    {
                        
                        Forcedfinish();
                        MaterialUseReport_dll();
                        ToolUseReport_dll();
                        CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 42192, 0);//hd1104
                        CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 42192, 0);//hd1104
                        CsPublicVariablies.Eap_run = false;
                        CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 6, false);
                    }
                }
            }
        }
        private void forced_finish_Click(object sender, RoutedEventArgs e)
        {
          
            //判断按钮是否需要生效
            Forcedfinish();
            MaterialUseReport_dll();
            ToolUseReport_dll();
            CsPublicVariablies.modbus_TCP.WriteSingleRegister_(1, 42192, 0);
            CsPublicVariablies.Eap_run = false;
            CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 6, false);
        }

        private void ToolUseReport_dll()//工具使用报告
        {
            ToolUseReport toolUseReport  = new ToolUseReport();
            toolUseReport.Body.UnitID = CsPublicVariablies.tool_MaterialModel.User_id;
            toolUseReport.Body.ToolID = CsPublicVariablies.tool_MaterialModel.Tool_id;
            toolUseReport.Body.LotID = CsPublicVariablies.produce_taskModel.Formula_batch_run;
            toolUseReport.Body.ToolQTY = CsPublicVariablies.vhd1100[5].ToString();
            HttpHelp.Post(toolUseReport, CsPublicVariablies.Eap_url, "application/json");
        }
        private  void MaterialUseReport_dll()//物料使用报告
        {
            MaterialUseReport materialUseReport = new MaterialUseReport();
            materialUseReport.Body.UnitID = CsPublicVariablies.tool_MaterialModel.User_id;
            materialUseReport.Body.MaterialID = CsPublicVariablies.tool_MaterialModel.Material_id;
            materialUseReport.Body.LotID =CsPublicVariablies.produce_taskModel.Formula_batch_run;
            materialUseReport.Body.SequenceNO = CsPublicVariablies.vhd1100[4].ToString();
            materialUseReport.Body.MaterialQTY = ConvertDataType.US_Float(CsPublicVariablies.vhd1100[12], CsPublicVariablies.vhd1100[13]);

            HttpHelp.Post(materialUseReport, CsPublicVariablies.Eap_url, "application/json");
        }
        private void Forcedfinish()//完工
        {
            EQPCompletedReport eQPCompletedReport  = new EQPCompletedReport();
            eQPCompletedReport.Body.LotID = CsPublicVariablies.produce_taskModel.Formula_batch_run;
            eQPCompletedReport.Body.PanelQTY = CsPublicVariablies.vhd1100[4].ToString();
            eQPCompletedReport.Body.Judge = 1;
            HttpHelp.Post(eQPCompletedReport, CsPublicVariablies.Eap_url, "application/json");
        }

        private void upkeep_Click(object sender, RoutedEventArgs e)
        {
            if (CsPublicVariablies.vm0[1203])
            {
                CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1,1203,false);
            }
            else
            {
                CsPublicVariablies.modbus_TCP.WriteSingleCoil_(1, 1203, true);
            }
        }
    }
}
