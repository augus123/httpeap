﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace YDET_EQP
{
    /// <summary>
    /// ShowPrintAlm.xaml 的交互逻辑
    /// </summary>
    public partial class ShowPrintAlm : Window
    {
        public ShowPrintAlm()
        {
            InitializeComponent();
        }

        private Timeline CreateDoubleAimation(UIElement element, bool autoReverse, RepeatBehavior repeat, string propertyPath, double from_, double by)
        {
            DoubleAnimation da = new DoubleAnimation();
            element.RenderTransform = new TranslateTransform();//支持位移
            da.From = from_;
            da.To = by;
            da.Duration = TimeSpan.FromSeconds(5);
            da.RepeatBehavior = repeat;
            da.AutoReverse = autoReverse;
            Storyboard.SetTarget(da, element);
            Storyboard.SetTargetProperty(da, new PropertyPath(propertyPath));
            return da;
        }
        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
         
            this.Close();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(CreateDoubleAimation(benStart, false, RepeatBehavior.Forever, "(UIElement.RenderTransform).(TranslateTransform.X)", 0, 260));
            storyboard.Begin();
          
        }
    }
}
