﻿using HTTP_Demo;
using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YDET_EQP.Models.LotInfoDownloadCommand;
using YDET_EQP.Models.LotInfoRequest;
using YDET_EQP.Properties;
using YDET_EQP.ViewModels;

namespace YDET_EQP.Views
{
    /// <summary>
    /// Produce_task.xaml 的交互逻辑
    /// </summary>
    public partial class Produce_task : Window
    {
      
        public Produce_task(Produce_taskModel produce_)
        {
            InitializeComponent();
            this.DataContext = produce_;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
        List<LotList> list = new List<LotList>();
    
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            list.Clear();
            list.Add(new LotList() { id = 0, str = "LOT ID" });
            list.Add(new LotList() { id = 1, str = "托盘 ID" });
            list.Add(new LotList() { id = 2, str = "Port ID" });
            list.Add(new LotList() { id = 3, str = "配料单 ID" });
            list.Add(new LotList() { id = 4, str = "批管卡 ID" });
            list.Add(new LotList() { id = 5, str = "产品型号 ID" });
            list.Add(new LotList() { id = 6, str = "A面" });
            list.Add(new LotList() { id = 7, str = "B面" });
            list.Add(new LotList() { id = 8, str = "挂篮" });
            LOT.ItemsSource = list;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void task_post_Click(object sender, RoutedEventArgs e)
        {
          
        }

        private void LOT_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
